package mkdercode.View;

import mkdercode.Trace.Logger;
import mkdercode.Trace.Metrics;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public abstract class Console {

	/**
	 * Sortie normale
	 * 	La normale est vouée a devenir plus grosse
	 * Sortie d'erreur
	 * @param str
	 */

	public static void printError(String str, int exitCode) {
        Metrics.stop_EXEC_TIME();
        System.out.println("Error : "+str);
        Console.displayMetrics();
        System.exit(exitCode);
	}
	//pour le debug
	public static void print(String str){
		System.out.println(str);
	}
	public static void append(String str){
        System.out.print(str);
    }
    public static  void printImage(BufferedImage image,String path){
        try {
            File file =  new File(path);
            ImageIO.write(image, "BMP", file);
        } catch (IOException e) {
            System.out.println("error :"+e.getMessage());
        }
    }

	//retourne un code d'erreur dans le shell
    public static void stop(int exitCode){
        Console.displayMetrics();
        System.exit(exitCode);
    }

    private static void displayMetrics() {
        Console.print(Metrics.display());
        Logger.end();
    }


}