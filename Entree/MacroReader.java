package mkdercode.Entree;

import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Sert pour le Parsing des definitions et appels de Macro.
 * La Map macros permet de stocker les Macros déjà definies afin de
 * pouvoir les reconnaitres en cas d'appel.
 * @author Ken, Elie
 */
public class MacroReader {
    private Map<String, Macro> macros;
    private String currentName;
    private ArrayList<String> macroBody;

    public MacroReader(){
        macros = new HashMap<>();
        macroBody = new ArrayList<>();
    }

    public void put(Macro macro){
        macros.put(macro.getName(),macro);
    }

    public Macro get(String macro){
        return macros.get(macro);
    }

    public boolean isMacro(String macro){
        return macros.containsKey(macro);
    }

    /**
     * Methodes nameMacro : Parse la premier ligne de la definition.
     * Elle recupere le nom qui est stocké dans currentName et creer une List macroBody
     * qui servira à stocké les Strings rencontrés tant que l'on a pas rencontré l'instruction
     * 'end'.
     * Retourne un boolean qui est vrai si la definition est fini (par ex, Macro definie sur une ligne)
     * et sinon faux.
     * @param line Ligne à parser.
     * @return Retourne la procedure si celle-ci est totalement définie sinon null.
     */
    public Macro nameMacro(List<String> line) throws BnfkSyntaxException{
        if(!checkIsValidDefineLine(line))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + line.get(0));
        if(!checkIsValideName(line.get(1)))
            throw new BnfkSyntaxException(BnfkSyntaxException.NAME_FORBIDDEN + line.get(1));

        macroBody.clear();
        currentName = line.get(1);
        if(line.size() > 3){
            line = removeRange(line, 0, 3);
            return defineMacro(line);
        }
        return null;
    }

    /**
     * Methode defineMacro : Parse une ligne pour l'ajouter à la macro.
     * Si l'instruction 'end' est recontrée alors on ajoute cette Macro à la Map.
     * Si une autre Macro est rencontrée on ajoute son contenu à notre Macro.
     * Renvoie vrai si la macro est finie sinon faux.
     * @param line Ligne à parser.
     * @return Retourne la procedure si celle-ci est totalement définie sinon null.
     */
    public Macro defineMacro(List<String> line) throws BnfkSyntaxException {
        if(line.isEmpty())
            return null;
        if(Directives.END.isTheDirective(line.get(line.size() - 1))){
            addToMacroBody(line, 0, line.size() - 1);
            Macro macro = new Macro(currentName, (ArrayList<String>) macroBody.clone());
            put(macro);
            return macro;
        }
        if(isMacro(line.get(0)))
            macroBody.addAll(readMacro(line));
        else
            addToMacroBody(line, 0, line.size());
        return null;
    }

    /**
     * Methode readMacro : Parse la ligne d'appel d'une macro afin de voir si un
     * parametre est donné.
     * Puis elle renvoie une liste de mot qui est le contenu de cette macro
     * multiplié par le nombre de repetitions données par le parametre.
     * @param line Ligne à parser.
     * @return Retourne le contenu de la Macro.
     */
    public ArrayList<String> readMacro(List<String> line) throws BnfkSyntaxException{
        if(!checkIsValidMacroLine(line))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + line.get(0));
        ArrayList<String> macroBody = new ArrayList<>();
        int parametre = 1;
        if(line.size() == 2)
            parametre = Integer.parseInt(line.get(1));
        for(int i = 0; i < parametre; i++){
            macroBody.addAll(get(line.get(0)).getBody());
        }
        return macroBody;
    }

    private boolean isInteger(String s){
        try{
            Integer.parseInt(s);
        }catch (NumberFormatException e){
            return false;
        }catch (NullPointerException e){
            return false;
        }
        return true;
    }

    //Methodes qui assure le validité de certain element necessaire aux methodes
    public boolean checkIsValidMacroLine(List<String> line){
        return line.size() == 1 || (line.size() == 2 && isInteger(line.get(1)));
    }

    private boolean checkIsValidDefineLine(List<String> line){
        return line.size() >= 3 && line.get(2).equals(":");
    }

    private boolean checkIsValideName(String name){
        return !Directives.exists(name) && !isInteger(name) && !name.equals(":") &&!isMacro(name);
    }

    //Methodes qui agissent sur des listes
    private void addToMacroBody(List<String> list, int from, int to){
        String str = listToString(list, from, to);
        if(!str.isEmpty())
            macroBody.add(str);
    }

    private List<String> removeRange(List<String> list, int from, int to){
        for(int i = from; i < to; i++){
            list.remove(from);
        }
        return list;
    }

    private String listToString(List<String> list, int from, int to){
        String word = "";
        for(int i = from; i < to; i++){
            word += " " + list.get(i);
        }
        return word;
    }
}