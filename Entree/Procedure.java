package mkdercode.Entree;

/**
 * Procedure contient le nom, le nombre de parametre ainsi que la valeur des
 * parametres en cas d'appel.
 * @author : Rachida
 */
public class Procedure {
    private String name; //Nom de la procedure
    private int nbParametre;
    private int[] valParametre;

    public Procedure(String name, int nbParametre){
        this.name = name;
        this.nbParametre = nbParametre;
    }

    public Procedure(String name, int[] valParametre){
        this.name = name;
        this.valParametre = valParametre;
    }

    public String getName(){
        return name;
    }

    public int getNbParametre(){
        return nbParametre;
    }

    public int[] getValParametre(){
        return valParametre;
    }
}
