package mkdercode.Entree;

import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkSyntaxException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * La Classe LineParser sert à parser le code reçu.
 * Pour chaque ligne, elle analysera le premier mot pour
 * appliquer la bonne methode.
 * Les queues lineBuffer et directivesBuffer permettent de garder
 * en memoire au cas où la Classe aurait recuperer plus d'une instruction
 * (en cas s'instructions courtes ou bien si on recupere le texte d'une Macro).
 * @author Elie
 */
public class LineParser {
    private LinkedBlockingQueue<String> lineBuffer;
    private LinkedBlockingQueue<Directives> directivesBuffer;
    private Pattern pattern;
    protected ProcedureReader procedureReader;
    protected MacroReader macroReader;
    protected boolean inMacro;

    public LineParser() {
        inMacro = false;
        lineBuffer = new LinkedBlockingQueue<>();
        directivesBuffer = new LinkedBlockingQueue<>();
        pattern = Pattern.compile(":|[^ |\n|\t|\r|:]+"); //Le pattern supprime tous les espaces et passages à la ligne
        procedureReader = new ProcedureReader();
        macroReader = new MacroReader();
    }

    /**
     * Parse la ligne passée en parametre.
     * Puis si on est dans une Macro renvoie immediatement la ligne parsée à celle-ci.
     * Sinon l'envoie à l'analyse.
     */
    public void parse(String line) throws BnfkSyntaxException {
        if (line == null) {
            directivesBuffer.add(Directives.VOID);
            return;
        }
        if (line.isEmpty())
            return;
        Matcher matcher = pattern.matcher(line);
        List<String> lineParsed = new ArrayList<>();
        while (matcher.find()) {
            lineParsed.add(matcher.group());
        }
        if (inMacro) {
            setDefineMacro(lineParsed);
        } else
            analyseFirstWord(lineParsed);
    }

    /**
     * Analyse le Premier mot de la liste
     * pour savoir comment agir avec la liste.
     * Les Directives obtenues sont stockées dans le directivesBuffer.
     * Si on obtient des lignes supplementaires (à cause des Macros), elles seront
     * stockées dans lineBuffer pour être parser ultérieurement.
     */
    private void analyseFirstWord(List<String> lineParsed) throws BnfkSyntaxException {
        String firstWord = lineParsed.get(0);
        directivesBuffer.clear();
        //@return [ARG]
        if (Directives.RETURN.isTheDirective(firstWord)) {
            setReturn(lineParsed);
        }
        //@define nomMacro :
        else if (Directives.DEFINE.isTheDirective(firstWord)) {
            setNameMacro(lineParsed);
        }
        //nomMacro [ARG]
        else if (macroReader.isMacro(firstWord)) {
            setReadMacro(lineParsed);
        }
        //@function nomFunction nbParametres :
        else if (Directives.FUNCTION.isTheDirective(firstWord)) {
            setDefineFunction(lineParsed);
        } else if (Directives.PROCEDURE.isTheDirective(firstWord)) {
            setDefineProcedure(lineParsed);
        }
        //nomFunction [ARG1 ARG2 ...]
        else if (procedureReader.isProcedure(firstWord)) {
            setReadProcedure(lineParsed);
        } else if (Directives.isLong(firstWord)) {
            setLongInstruction(lineParsed);
        } else {
            setShortInstruction(lineParsed);
        }
    }

    /**
     * Verifie qu'il y a une Directives à renvoyer avec getNextDirective.
     */
    public boolean find() {
        return !(lineBuffer.isEmpty() && directivesBuffer.isEmpty());
    }

    /**
     * Retourne la prochaine Directives contenue dans directivesBuffer.
     * Si il est vide, il parse une nouvelle ligne de lineBuffer puis retourne la/les nouvelle Directives
     * obtenue.
     */
    public Directives getNextDirective() throws BnfkSyntaxException {
        while (directivesBuffer.isEmpty() && !lineBuffer.isEmpty())
            parse(lineBuffer.remove());
        return directivesBuffer.remove();
    }

    //Methodes pour analyseFirstWord
    protected void setReturn(List<String> lineParsed) throws BnfkSyntaxException {
        int parametre = procedureReader.getReturn(lineParsed);
        Directives.RETURN.setParametres(parametre);
        directivesBuffer.add(Directives.RETURN);
    }

    protected void setNameMacro(List<String> lineParsed) throws BnfkSyntaxException {
        if(alreadyExist(lineParsed))
            throw new BnfkSyntaxException(BnfkSyntaxException.ALREADY_EXIST + lineParsed.get(1));
        if (macroReader.nameMacro(lineParsed) == null)
            inMacro = true;
    }

    protected void setDefineMacro(List<String> lineParsed) throws BnfkSyntaxException {
        if (macroReader.defineMacro(lineParsed) != null)
            inMacro = false;
    }

    protected void setReadMacro(List<String> lineParsed) throws BnfkSyntaxException {
        ArrayList<String> macroBody = macroReader.readMacro(lineParsed);
        lineBuffer.addAll(macroBody);
    }

    protected void setDefineFunction(List<String> lineParsed) throws BnfkSyntaxException {
        if(alreadyExist(lineParsed))
            throw new BnfkSyntaxException(BnfkSyntaxException.ALREADY_EXIST + lineParsed.get(1));
        Procedure procedure = procedureReader.nameProcedure(lineParsed);
        Directives.PROCEDURE_DEFINE.setParametres(procedure);
        directivesBuffer.add(Directives.PROCEDURE_DEFINE);
    }

    protected void setDefineProcedure(List<String> lineParsed) throws BnfkSyntaxException {
        if(alreadyExist(lineParsed))
            throw new BnfkSyntaxException(BnfkSyntaxException.ALREADY_EXIST + lineParsed.get(1));
        Procedure procedure = procedureReader.nameProcedure(lineParsed);
        Directives.PROCEDURE_DEFINE.setParametres(procedure);
        directivesBuffer.add(Directives.PROCEDURE_DEFINE);
    }

    protected void setReadProcedure(List<String> lineParsed) throws BnfkSyntaxException {
        Procedure procedure = procedureReader.readProcedure(lineParsed);
        Directives.PROCEDURE_CALL.setParametres(procedure);
        directivesBuffer.add(Directives.PROCEDURE_CALL);
    }

    protected void setLongInstruction(List<String> line) throws BnfkSyntaxException {
        if (!checkIsValidLongInstruction(line))
            throw new BnfkSyntaxException(BnfkSyntaxException.ONLY_ONE_INSTRUCTION + line.get(0));
        directivesBuffer.add(Directives.toDirective(line.get(0)));
    }

    private boolean checkIsValidLongInstruction(List<String> line) {
        return line.size() == 1;
    }

    protected void setShortInstruction(List<String> line) throws BnfkSyntaxException {
        for (String word : line) {
            for (int i = 0; i < word.length(); i++) {
                if (!Directives.isShort((String.valueOf(word.charAt(i)))))
                    throw new BnfkSyntaxException();
                directivesBuffer.add(Directives.toDirective(String.valueOf(word.charAt(i))));
            }
        }
    }

    private boolean alreadyExist(List<String> line) {
        return line.size() > 1 && (macroReader.isMacro(line.get(1)) || procedureReader.isProcedure(line.get(1)));
    }
}