package mkdercode.Entree;

import java.io.*;

import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkFileNotFoundException;
import mkdercode.Exception.BnfkSyntaxException;
import mkdercode.Trace.Metrics;

/**
 * La Classe CodeReader lit le fichier donné en parametre ligne par ligne et
 * les envoies à un parser LineParser afin d'obtenir des Directives lisibles par
 * l'ensemble de la machine virtuelle.
 * @author mxm, Elie, Ken, Rachida
 */
public class CodeReader implements ReaderInterface {
    private int lineNb;  //Ligne actuelle
    private BufferedReader reader;
    private String path; //Utile pour les erreurs
    private LineParser lineParser;

    public CodeReader(String path) throws BnfkException {
        lineParser = new LineParser();
        lineNb = 0;
        this.path = path;
        try {
            reader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            throw new BnfkFileNotFoundException(path);
        }
    }

    /**
     * Lit la ligne dans le fichier et la renvoie.
     * Envoie null si le fichier est fini.
     * Compte aussi le numero de la ligne et supprime les commentaires.
     * @return
     * @throws BnfkFileNotFoundException
     */
    private String readFile() throws BnfkFileNotFoundException {
        String line;
        try {
            line = reader.readLine();
            if (line == null) {
                reader.close();
                return null;
            }

            lineNb++;
            line = removeComment(line);
            return line;
        } catch (IOException e) {
            throw new BnfkFileNotFoundException(path);
        }
    }

    /**
     * Tant que LineParser ne renvoie rien on lui recupere les lignes du fichier
     * qu'on envoie au parser.
     * @return
     * @throws BnfkException
     */
    public Directives getNextDirective() throws BnfkException {
        try {
            while (!lineParser.find()) {
                lineParser.parse(readFile());
            }
            return toDirective(lineParser.getNextDirective());
        } catch (BnfkSyntaxException e) {
            throw new BnfkSyntaxException(lineNb, e.getMessage());
        }
    }

    private Directives toDirective(Directives d) {
        if (d == Directives.VOID)
            return null;
        else{
            Metrics.incr_PROG_SIZE();
            return d;
        }
    }

    private String removeComment(String line) {
        if (line.contains("#")) return line.substring(0, line.indexOf("#"));
        return line;
    }
}