package mkdercode.Entree;

import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkException;

public interface ReaderInterface {

    Directives getNextDirective() throws BnfkException;
}