package mkdercode.Entree;

import mkdercode.Constante.Config;
import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkFileNotFoundException;
import mkdercode.View.Console;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * La Classe ImageManager s'occupe à la fois de la lecture d'image
 * mais aussi de la traduction de programme Bf sous forme d'image.
 * @author mxm, Ken
 */
public class ImageManager implements TranslatorInterface, ReaderInterface {
    private BufferedImage image;
    private File file;

    private Graphics g;
    private int x;
    private int y;

    private int width;
    private int height;
    private String source;
    private String destination;

    private ArrayList<Directives> lineList;
    private int pointer;

    public ImageManager(String path) {
        image = null;
        file = null;
        g = null;
        x = 0;
        y = 0;
        width = 0;
        height = 0;
        source = path;
        destination = path.split("\\.", 2)[0] + ".bmp"; //Remplace l'extension du fichier

        lineList = new ArrayList<>();
        pointer = 0;
    }


    /**
     * @param instructionList
     * @param totalNumberOfInstruction
     * Create the buffured image in the good dimensions and fill it with the directives
     */
    public void translate(ArrayList<Directives> instructionList, int totalNumberOfInstruction) {
        int compteurCourant = 0;

        if (image == null) {
            //Creer l'image vide aux bonnes dimensions
            width = (int) Math.round(Math.sqrt(totalNumberOfInstruction));
            height = 0;
            if (Math.sqrt(totalNumberOfInstruction) > width) {
                height = width + 1;
            } else {
                height = width;
            }
            image = new BufferedImage(width * Config.INSTR_SIZE, height * Config.INSTR_SIZE, BufferedImage.TYPE_INT_RGB);

            g = image.getGraphics();

            g.setColor(Color.BLACK);//Tout a 0
            g.fillRect(0, 0, width * Config.INSTR_SIZE, height * Config.INSTR_SIZE);

        }

        //Ecrire 3 par 3
        if (instructionList.size() != 0) {
            while (compteurCourant < totalNumberOfInstruction) {
                Directives line = instructionList.get(compteurCourant);

                g.setColor(Color.decode(line.getRGB()));

                g.fillRect(x, y, Config.INSTR_SIZE, Config.INSTR_SIZE);//cree un rectangle
                compteurCourant++;

                //retour a la ligne
                if (x == width * Config.INSTR_SIZE - Config.INSTR_SIZE) y += Config.INSTR_SIZE;
                if (x < width * Config.INSTR_SIZE - Config.INSTR_SIZE)
                    x += Config.INSTR_SIZE;
                else x = 0;
            }
        }
    }

    /**
     * Create the .bmf file from the buffured image
     */
    public void display() {
        Console.printImage(image, destination);
        g.dispose();
    }

    public ArrayList<Directives> getNext() throws BnfkException {

        try {
            file = new File(source);
            image = ImageIO.read(file);
        } catch (IOException e) {
            throw new BnfkFileNotFoundException(source);
        }

        ArrayList<Directives> res = new ArrayList<>();
        Color pixelValue;
        height = image.getHeight();
        width = image.getWidth();

        while (!isEndoffile()) {
            pixelValue = new Color(image.getRGB(x, y));
            res.add(Directives.toDirective(toRGB(pixelValue).toUpperCase()));
            if (x == width - Config.INSTR_SIZE) y += Config.INSTR_SIZE;
            if (x < width - Config.INSTR_SIZE)
                x += Config.INSTR_SIZE;
            else x = 0;

        }
        return res;
    }

    /**
     *
     * @return
     * @throws BnfkException
     * Fill an arraylist of directives by reading an image 3 pixels by 3 pixels
     */
    @Override
    public Directives getNextDirective() throws BnfkException {
        pointer++;
        if (pointer >= lineList.size()) {
            lineList = getNext();
            if (lineList.size() == 0) {
                return null;
            }
            pointer = 0;
            return lineList.get(pointer);
        } else {
            return lineList.get(pointer);
        }
    }

    /**
     *
     * @param color
     * @return String : the hexadecimal form of the an object color
     */
    private String toRGB(Color color) {
        return "#" + Integer.toHexString(color.getRGB()).substring(2);
    }

    /**
     * It's the end of file when the last pixel is reached or when the current pixel black
     * @return boolean
     */
    public boolean isEndoffile() {
        return (new Color(image.getRGB(x, y)).equals(Color.BLACK) || x == width && y == height);
    }
}