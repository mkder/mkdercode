package mkdercode.Entree;

import mkdercode.Constante.Config;
import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Interpreteur des procedures.
 * Les procedures déjà existantes sont stockées dans une HashMap.
 * @author : Rachida
 */
public class ProcedureReader {
    private HashMap<String, Procedure> listProcedure;

    public ProcedureReader() {
        listProcedure = new HashMap<>();
    }

    public void put(Procedure procedure) {
        listProcedure.put(procedure.getName(), procedure);
    }

    public Procedure get(String name) {
        return listProcedure.get(name);
    }

    public boolean isProcedure(String procedure) {
        return listProcedure.containsKey(procedure);
    }

    /**
     * Parse la ligne de definition de la procedure.
     * On verifie la validité du nom, le nombre de parametre et le caractere ':'.
     * Puis on retourne la procedure créée.
     * @param line
     * @return
     * @throws BnfkSyntaxException
     */
    public Procedure nameProcedure(List<String> line) throws BnfkSyntaxException{
        if(!checkIsValidDefineLine(line))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + line.get(0));
        if(!checkIsValidName(line.get(1)))
            throw new BnfkSyntaxException(BnfkSyntaxException.NAME_FORBIDDEN + line.get(1));

        Procedure procedure = new Procedure(line.get(1), Integer.parseInt(line.get(2)));
        put(procedure);
        return procedure;
    }

    /**
     * Parse la ligne d'appel d'une procedure.
     * On verifie que le nombre d'arguments est bien celui de la procedure appellée.
     * Puis on renvoie un objet Procedure contenant les valeurs de ces arguments.
     * @param line
     * @return
     * @throws BnfkSyntaxException
     */
    public Procedure readProcedure(List<String> line) throws BnfkSyntaxException{
        if(!checkIsValidCallLine(line))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + line.get(0));
        int nbParametres = get(line.get(0)).getNbParametre();
        List<Integer> parametreVal = new ArrayList<>();
        for(int i = 1; i < nbParametres + 1; i++){
            if(!isInteger(line.get(i)))
                throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + line.get(0));
            parametreVal.add(Integer.parseInt(line.get(i)));
        }
        return new Procedure(line.get(0), toArray(parametreVal));
    }

    /**
     * Parse la ligne de l'instruction @return.
     * On verifie si on a un parametre; si c'est le cas on verifie que c'est
     * un entier et on le renvoie.
     * @param line
     * @return
     * @throws BnfkSyntaxException
     */
    public int getReturn(List<String> line) throws BnfkSyntaxException {
        if(!checkValidReturnLine(line))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + line.get(0));
        int parametreValue = -Config.MEMORY_SIZE - 1; //Pour etre sure d'etre sortie de la memoire
        if(line.size() > 1)
            parametreValue = Integer.parseInt(line.get(1));
        return parametreValue;
    }

    private boolean checkIsValidDefineLine(List<String> line){
        return line.size() == 4 && isInteger(line.get(2)) && line.get(3).equals(":");
    }

    private boolean checkIsValidName(String name){
        return !Directives.exists(name) && !isInteger(name) && !name.equals(":") && !isProcedure(name);
    }

    private boolean checkIsValidCallLine(List<String> line){
        int nbParametre = get(line.get(0)).getNbParametre();
        return line.size() == nbParametre + 1;
    }

    private boolean checkValidReturnLine(List<String> line){
        return line.size() == 1 || (line.size() == 2 && isInteger(line.get(1)));
    }

    private boolean isInteger(String word) {
        try {
            Integer.parseInt(word);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return true;
    }

    private int[] toArray(List<Integer> list) {
        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }
}