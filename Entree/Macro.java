package mkdercode.Entree;

import java.util.ArrayList;

/**
 * @author : Ken
 * This class define the Macro
 * Here is an example of macro :
 * @define Test +++++ end
 * Test
 */

public class Macro {
    private String name;
    private ArrayList<String> body;

    public Macro(String name, ArrayList<String> body){
        this.name = name;
        this.body = body;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<String> getBody(){
        return body;
    }
}
