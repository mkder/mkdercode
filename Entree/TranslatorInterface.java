package mkdercode.Entree;

import mkdercode.Constante.Directives;

import java.util.ArrayList;

/**
 * CLASSE :
 *
 * @author mxm
 */
public interface TranslatorInterface {
    void translate(ArrayList<Directives> instructionList, int totalNumberOfInstruction);
    void display();

}
