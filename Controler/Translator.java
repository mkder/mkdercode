package mkdercode.Controler;

import mkdercode.Constante.Config;
import mkdercode.Constante.Directives;
import mkdercode.Entree.CodeReader;
import mkdercode.Entree.ImageManager;
import mkdercode.Entree.ReaderInterface;
import mkdercode.Entree.TranslatorInterface;
import mkdercode.Exception.BnfkException;
import mkdercode.View.Console;

import java.util.ArrayList;

/**
 * Translator est l'executable s'occupant de la traduction du programme Bf
 * en image.
 * Il recupère grâce au ReaderInterface les instructions sous forme de Directives
 * puis les ajoutes à une liste.
 * Enfin le travail de traduction lui-même est donné à ImageManager.
 */
public class Translator implements ExecutableInterface {
    private ReaderInterface reader;
    private TranslatorInterface writer;


    public Translator(String path) {
        try {
            this.reader = new CodeReader(path);
            this.writer = new ImageManager(path);
        } catch (BnfkException e) {
            Console.printError(e.getMessage(), e.getExitCode());
        }
    }

    @Override
    public void exec() {
        ArrayList<Directives> totalList = new ArrayList<>();
        Directives directives;
        long compteur = 0;

        int compteurLocal = 0;
        try {
            while ((directives = reader.getNextDirective()) != null) {
                if (compteur >= Integer.MAX_VALUE) {
                    throw new BnfkException("Le fichier en entrée ne peut-être bufferisé.");
                } else {
                    if (directives.getRGB() == null)
                        throw new BnfkException("Le programme contient des instructions intraduisibles.");
                    totalList.add(directives);
                    compteur++;
                }
            }
            writer.translate(totalList, (int) compteur);
        } catch (BnfkException e) {
            Console.printError(e.getMessage(), e.getExitCode());
        }
        writer.display();
    }
}
