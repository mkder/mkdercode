package mkdercode.Controler;

import mkdercode.Constante.Directives;
import mkdercode.Exception.*;
import mkdercode.Model.*;
import mkdercode.Model.CommandFactory;
import mkdercode.Trace.Logger;
import mkdercode.View.Console;

/**
 * Interpretor est la Classe executable appelée par defaut.
 * Elle sert à executer le programme Bf.
 * Elle fait appel à un ReaderInterface pour la lecture du programme Bf,
 * un MemoryInterface pour le stockage des variables et
 * une InstructionsStack pour le sotckage des instructions.
 * <p>
 * Les instructions sont obtenues sous forme de Directives traduite ensuite
 * en Command par CommandFactory avant d'être executées dans la methode exec.
 */
public class Interpretor implements ExecutableInterface, LoggerInterface {
    private MemoryInterface memory;
    private InstructionsStack stack;
    private CommandFactory commandFactory;

    private boolean log = false;

    public Interpretor(String path) {
        this.memory = new Memory();

        try {
            stack = new InstructionsStack(path);
        } catch (BnfkException e) {
            Console.printError(e.getMessage(), e.getExitCode());
        }
        this.commandFactory = new CommandFactory(memory, stack);
    }

    /**
     * Boucle l'execution des Commands jusqu'à la Directives null
     * qui indique le bout de la stack d'instructions.
     */
    public void exec() throws BnfkException {
        Directives directives = null;

        while ((directives = stack.getNext()) != null) {
            Command command = commandFactory.create(directives);
            command.exec();
            if (log) Logger.log(command);
        }

        Console.print(memory.toString());
    }

    public void setLogOn() {
        log = true;
        Logger.init();
    }
}