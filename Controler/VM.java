package mkdercode.Controler;


import mkdercode.CodeGenerator.CodeGenerator;
import mkdercode.CodeGenerator.CodeGeneratorC;
import mkdercode.CodeGenerator.CodeGeneratorJ;
import mkdercode.Constante.Arguments;
import mkdercode.Constante.Config;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkFileNotFoundException;
import mkdercode.Exception.BnfkWrongArgException;
import mkdercode.View.Console;
import mkdercode.Trace.Metrics;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;


/**
 * CLASSE : Anciennement Engine, VM correspond a virtual machine.
 * Elle utilise une HashMap pour connaitre les arguments utilisés lors du
 * démarrage de la machine virtuelle.
 * Puis suivant ces arguments elle va executer les ExecutableInterface correspondants.
 * C'est le point d'entrée du programme
 * @author mxm, elie
 *         10/10/16.
 */
public class VM {
    private ExecutableInterface exe;
    private String path = "";
    private HashMap<String, Boolean> listArg;
    private boolean hasToLog = false;
    private LoggerInterface log;

    public static void main(String argv[]){
        VM vm = new VM();
        vm.execute(argv);
        vm.stop();
    }

    public void execute(String arg[]){
        try {
            exec(arg);
        }catch (BnfkException e){
            Console.printError(e.getMessage(), e.getExitCode());
        }
    }

    public void stop(){
        Console.stop(0);
    }

    /**
     * Lance l'ExecutableInterface actuel.
     */
    private void exec() throws BnfkException{
        exe.exec();
        if(hasToLog && log!=null);
        Metrics.stop_EXEC_TIME();
    }

    //le constructeur de la machine virtuelle
    public VM(){
        log = null;
        Metrics.start_EXEC_TIME();//S12
        listArg = new HashMap<>();
    }
    /**
     * Retourne le chemin vers le fichier, placé après l'option -p
     * @param args Liste des arguments
     * @param i Index sur lequel devrait se trouver le chemin
     * @throws BnfkWrongArgException Au cas où l'element attendu n'existe pas.
     */
    private void argReadFile(String[] args, int i) throws BnfkWrongArgException {
        try{
            path = args[i];
        }catch (ArrayIndexOutOfBoundsException e){
            throw new BnfkWrongArgException(Arguments.READFILE);
        }
    }

    /**
     * Analyse la liste des arguments mot par mot.
     * @param argv Liste des arguments.
     * @throws BnfkFileNotFoundException
     * @throws BnfkWrongArgException
     */
    private void getArg(String argv[]) throws BnfkFileNotFoundException, BnfkWrongArgException {
        for (int i = 0; i < argv.length; i++) {
            switch (argv[i]) {
                case Arguments.TRACE:
                    hasToLog=true;
                    break;
                case Arguments.READFILE:
                    i++;
                    argReadFile(argv, i);
                    break;
                case Arguments.REWRITE:
                    listArg.put(Arguments.REWRITE, true);
                    break;
                case Arguments.TRANSLATE:
                    listArg.put(Arguments.TRANSLATE, true);
                    break;
                case Arguments.CHECK:
                    listArg.put(Arguments.CHECK, true);
                    break;
                case Arguments.IN:
                    i++;
                    getInputFile(argv, i);
                    break;
                case Arguments.OUT:
                    i++;
                    getOutputFile(argv, i);
                    break;
                case Arguments.GENERATE_C :
                    listArg.put(Arguments.GENERATE_C, true);
                    break;
                case Arguments.GENERATE_J :
                    listArg.put(Arguments.GENERATE_J, true);
                    break;
                default:
                    throw new BnfkWrongArgException(argv[i]);
            }
        }
    }

    //Les arguments rewrite et translate empeche l'execution mais pas entre elles

    /**
     * Execute les ExecutableInterface suivant les arguments trouvés dans la liste.
     * @param argv Liste d'arguments.
     * @throws BnfkException
     */
    public void exec(String[] argv) throws BnfkException{
        getArg(argv);
        if(listArg.size() == 0){
            exe = new Interpretor(path);
            if(hasToLog){
                log = (LoggerInterface) exe;
                log.setLogOn();
            }
            exec();
        }else {
            if(listArg.containsKey(Arguments.CHECK)){
                exe = new Checker(path);
                exec();
            }
            if(listArg.containsKey(Arguments.REWRITE)){
                exe = new Rewriter(path);
                exec();
            }
            if(listArg.containsKey(Arguments.TRANSLATE)){
                exe = new Translator(path);
                exec();
            }

            if(listArg.containsKey(Arguments.GENERATE_C)){
                exe = new CodeGeneratorC(path);
                exec();
            }

            if(listArg.containsKey(Arguments.GENERATE_J)){
                exe = new CodeGeneratorJ(path);
                exec();
            }

        }
    }

    private void getInputFile(String argv[], int i) throws BnfkWrongArgException, BnfkFileNotFoundException {
        try {
            Config.PATH_IN = new FileInputStream(argv[i]);
        }catch (NullPointerException e){
            throw new BnfkWrongArgException(Arguments.IN);
        } catch (IOException e) {
            throw new BnfkFileNotFoundException(argv[i]);
        }
    }

    private void getOutputFile(String argv[], int i) throws BnfkWrongArgException, BnfkFileNotFoundException {
        try {
            Config.PATH_OUT = new FileOutputStream(argv[i]);
        }catch (NullPointerException e){
            throw new BnfkWrongArgException(Arguments.OUT);
        } catch (IOException e) {
            throw new BnfkFileNotFoundException(argv[i]);
        }
    }
    //retourne un code dans le shell

    //For testing
    public String getExeType(){
        return exe.getClass().getSimpleName();
    }

}
