package mkdercode.Controler;

import com.google.common.io.Files;
import mkdercode.Constante.Directives;
import mkdercode.Entree.CodeReader;
import mkdercode.Entree.ImageManager;
import mkdercode.Entree.ReaderInterface;
import mkdercode.Exception.BnfkBackInfinite;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkJumpInfinite;
import mkdercode.View.Console;

/**
 * Checker est la Classe verifiant la bonne construction du programme Bf.
 * Elle recupère les instructions sous forme de Directives
 * grâce au ReaderInterface.
 * Puis se utilise un Integer checkCount pour controler l'ouverture et fermeture
 * de parentheses JUMP - BACK.
 * checkCount s'incremente à chaque passe de JUMP et se decremente à chaque passage
 * de BACK.
 * A la fin, checkCount doit être egal à zero.
 * @author mxm
 */
public class Checker implements ExecutableInterface {
    private ReaderInterface reader;
    private int checkCount;
    private String path;

    public Checker(String path) {
        checkCount = 0;
        this.path = path;
        try {
            if (Files.getFileExtension(path).equals("bmp")) {
                this.reader = new ImageManager(path);
            } else {
                this.reader = new CodeReader(path);
            }
        } catch (BnfkException e) {
            Console.printError(e.getMessage(), e.getExitCode());
        }
    }

    @Override
    public void exec() throws BnfkException {
        Directives directives;
        while ((directives = reader.getNextDirective()) != null) {
            if (directives == Directives.JUMP) checkCount++;
            if (directives == Directives.BACK) checkCount--;
            if (checkCount < 0)
                throw new BnfkBackInfinite(); //Si on a un nombre negatif c'est qu'il y a plus de BACK que de JUMP
        }
        if (checkCount > 0)
            throw new BnfkJumpInfinite();
        else {
            Console.print("File " + path + " is well formed");
        }
    }

    public boolean isValid() {
        return checkCount == 0;
    }
}
