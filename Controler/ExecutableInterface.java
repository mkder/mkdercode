package mkdercode.Controler;

import mkdercode.Exception.BnfkException;

/**
 * Les Classes ExecutableInterface sont celle qui
 * vont controler l'ensemble du comportement de la machine virtuelle.
 * @author mxm
 */
public interface ExecutableInterface {
    /**
     * Sert à lancer l'execution.
     */
    void exec() throws BnfkException;
}
