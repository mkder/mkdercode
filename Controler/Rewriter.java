package mkdercode.Controler;

import com.google.common.io.Files;
import mkdercode.Constante.Directives;
import mkdercode.Entree.CodeReader;
import mkdercode.Entree.ImageManager;
import mkdercode.Entree.ReaderInterface;
import mkdercode.Exception.BnfkException;
import mkdercode.View.Console;

/**
 * Rewriter est l'executable s'occupant de la traduction du programme Bf
 * en version courte.
 * Il recupère grâce au ReaderInterface les instructions sous forme de Directives
 * puis ajoute sa version courte dans une String avant de l'afficher.
 * En cas d'instruction intraduisible (fonction, procedure...) il renvoie une erreur.
 * @author mxm
 */
public class Rewriter implements ExecutableInterface {
    private ReaderInterface reader;

    public Rewriter(String path) throws BnfkException {
        if (Files.getFileExtension(path).equals("bmp")) {
            this.reader = new ImageManager(path);
        } else {
            this.reader = new CodeReader(path);
        }
    }

    @Override
    public void exec() throws BnfkException {
        Directives directives;
        String res = "";
        while ((directives = reader.getNextDirective()) != null) {
            if (directives.getShort() == null)
                throw new BnfkException("Le programme possedent des instructions intraduisibles.");
            res += directives.getShort();
        }
        Console.append(res);
    }
}
