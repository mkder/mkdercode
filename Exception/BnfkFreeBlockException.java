package mkdercode.Exception;

/**
 * Erreur liée au SuperBlock de la memoire.
 * @author mxm
 */
public class BnfkFreeBlockException extends BnfkMemoryException {
    public BnfkFreeBlockException(){
        super("On ne peut pas decrementer une case libre");
    }
}
