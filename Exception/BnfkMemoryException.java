package mkdercode.Exception;

/**
 * Erreur liée à la Memoire.
 * @author mxm
 */
public class BnfkMemoryException extends BnfkException {
    public BnfkMemoryException(RuntimeException e) {
        //ca c'est pour recup toutes les exceptions de la memoire sous le meme nom
        super("MemoryException : " + e.getMessage());
    }
    public BnfkMemoryException(String e) {
        //ca c'est pour recup toutes les exceptions de la memoire sous le meme nom
        super("MemoryException : " + e);
    }
}
