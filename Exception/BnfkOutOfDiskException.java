package mkdercode.Exception;

/**
 * Erreur au cas où le pointeur memoire sortirait de celle-ci.
 * @author mxm
 */
public class BnfkOutOfDiskException extends BnfkMemoryException {
    public BnfkOutOfDiskException(String moreInfo){
        super("BnfkOutOfDiskException : Le pointeur ne peut pas se deplacer a l'exterieur de la memoire - "+moreInfo);
    }

    public int getExitCode(){
        return 2;
    }
}
