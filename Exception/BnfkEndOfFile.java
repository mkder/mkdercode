package mkdercode.Exception;

/**
 * Erreur au cas où on tenterai de lire un fichier fini.
 * @author Rachida
 */
public class BnfkEndOfFile extends BnfkException {
    public BnfkEndOfFile(String fonction){
        super("la fonction " + fonction + " a atteind le bout du fichier.");
    }
}
