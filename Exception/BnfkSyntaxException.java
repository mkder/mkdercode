package mkdercode.Exception;

/**
 * Les erreurs de syntaxe.
 * Peut donner la ligne où s'est produite l'erreur.
 * @author mxm
 */
public class BnfkSyntaxException extends BnfkException {
    final public static String NAME_FORBIDDEN = "Ce nom ne peut pas être donné : ";
    final public static String ALREADY_EXIST = "Le nom est déjà donné : ";
    final public static String ONLY_ONE_INSTRUCTION = "La commande doit être seule sur la ligne : ";
    final public static String CALL_INSTRUCTION = "Parametres incorrects : ";

    public BnfkSyntaxException() {super("Caractere incorrect.");}
    public BnfkSyntaxException(int line){
        super("Caractère incorrect a la ligne ligne :"+line);
    }
    public BnfkSyntaxException(String cause){
        super(cause);
    }
    public BnfkSyntaxException(int line, String cause){
        super("Erreur de syntaxe à la ligne " + line + " : " + cause);
    }
}
