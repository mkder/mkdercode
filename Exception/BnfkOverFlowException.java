package mkdercode.Exception;


/**
 * Erreur en cas de dépacement de capacité d'une case memoire.
 * @author mxm
 */
public class BnfkOverFlowException extends BnfkMemoryException{
    public BnfkOverFlowException() {
        super("Depassement de la capacité de représentation de la mémoire: nombre < 0 / nombre > 255");
    }

    public int getExitCode(){
        return 1;
    }
}
