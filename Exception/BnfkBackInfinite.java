package mkdercode.Exception;

/**
 * Erreur en cas d'instruction BACK seule.
 * @author mxm
 */
public class BnfkBackInfinite extends BnfkException{
    public BnfkBackInfinite() {
        super("La commande back a été appelée sans command jump associée");
    }

    public int getExitCode(){
        return 4;
    }
}
