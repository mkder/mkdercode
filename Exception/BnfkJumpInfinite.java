package mkdercode.Exception;

/**
 * Erreur en cas d'instruction JUMP seule.
 * @author Elie
 */
public class BnfkJumpInfinite extends BnfkException {
    public BnfkJumpInfinite(){
        super("La commande JUMP a été appelé mais aucune commande BACK ne lui est associée.");
    }

    public int getExitCode(){
        return 4;
    }
}
