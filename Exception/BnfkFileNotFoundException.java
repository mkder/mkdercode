package mkdercode.Exception;

/**
 * Erreur au cas où le fichier demandé est introuvable.
 * @author mxm
 */
public class BnfkFileNotFoundException extends BnfkException {
    public BnfkFileNotFoundException(String path){
        super("BnfkFileNotFoundException : Le fichier " + path + " est introuvable ou ne peut pas être modifié.");
    }

    public int getExitCode(){
        return 3;
    }
}
