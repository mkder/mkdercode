package mkdercode.Exception;

/**
 * Les erreurs personnalisées de la machine virtuelle.
 * Le code erreur par defaut est 10.
 * @author mxm
 */
public class BnfkException extends Exception {
    public BnfkException(){
        super();
    }
    public BnfkException(String a){
        super(a);
    }

    public int getExitCode(){
        //Code 10 est code erreur par defaut. Parce que
        return 10;
    }
}
