package mkdercode.Exception;

/**
 * Erreur liée au argument de démarrage.
 * @author mxm
 */
public class BnfkWrongArgException extends BnfkException {

    public BnfkWrongArgException(String arg){
        //TODO ajouter une option -h pour voir la liste des commandes
        super("L'option "+arg +" est mal définie ou inexistante.");
    }
}
