package mkdercode.Trace;

import mkdercode.Constante.Config;
import mkdercode.Exception.BnfkOutOfDiskException;
import mkdercode.Model.Command;
import mkdercode.View.Console;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * CLASSE :
 *
 * @author mxm
 *         25/11/16.
 */
public class Logger {
    private static BufferedWriter writer;//On en a qu'un
    private static boolean init=false;
    public static void init(){

        try {
            writer = new BufferedWriter(new FileWriter(Config.LOG_PATH));
        } catch (IOException e) {
            Console.printError("Log initialization failed : incorrect log path", 10);
        }
        try {
            writer.write(new Date()+" - Start \n");
        } catch (IOException e) {
            Console.printError("Log initialization failed : Can't write in "+Config.LOG_PATH, 10);
        }
        init=true;
    }
    public static void fail(String message){
        String line = "[failed] : "+message+"\n";
        try {
            writer.write(line);
        } catch (IOException e) {
            Console.printError("Programm failed - Log termination failed", 10);
        }
    }
    public static void log(Command c) throws BnfkOutOfDiskException{
        String line = "-[executed] | \t-"+c.getClass().getSimpleName()+"\t|\t-"+c.displayMemoryState()+"\n";
        try {
            writer.write(line);
        } catch (IOException e) {
            Console.printError("Log writing for ["+c+"]: "+c.getClass().getSimpleName()+" failed", 10);
        }
    }
    public static void end(){

        if(init){
            try {

                String line = "[stopped]\n";
                writer.write(line);
                writer.close();
            } catch (IOException e) {
                Console.printError("Log termination failed", 10);
            }
        }

    }
}
