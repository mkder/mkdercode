package mkdercode.Trace;

import java.util.Date;

/**
 * CLASSE : Metrics, contains the data to be displayed
 *
 * @author mxm
 *         24/11/16.
 */
public class Metrics {
    public static long start, end;
    /**
     * @attrib PROG_SIZE: the number of instructions in the program;
     */
    protected static long PROG_SIZE;
    /**
     * @attrib EXEC TIME: the execution time of the program, in milliseconds;
     */
    protected static long EXEC_TIME;
    /**
     * @attrib EXEC_MOVE: the number of times the execution pointer was moved to execute this program;
     */
    protected static long EXEC_MOVE;
    /**
     * @attrib DATA_MOVE: the number of time the data pointer was moved to execute this program;
     */
    protected static long DATA_MOVE;
    /**
     * @attrib DATA_WRITE: the number of time the data pointer was moved to execute this program;
     */
    protected static long DATA_WRITE;
    /**
     * @attrib DATA_RED:  the number of times the memory was accessed to read its contents (i.e., JUMP,
     * BACK, OUT)
     */
    protected static long DATA_READ;

    //Counters
    public static void incr_PROG_SIZE(){
        PROG_SIZE++;
    }
    public static void start_EXEC_TIME(){
        start = System.currentTimeMillis();
       // System.out.println("\n --timer started--\nexectime = "+start);
    }
    public static void stop_EXEC_TIME(){
        end = System.currentTimeMillis();
        EXEC_TIME = end - start;
       // System.out.println("\n --timer stopped--\nexectime = "+end);
    }
    public static void incr_EXEC_MOVE(){
        EXEC_MOVE++;
    }
    public static void incr_DATA_MOVE(){
        DATA_MOVE++;
    }
    public static void incr_DATA_WRITE(){
        DATA_WRITE++;
    }
    public static void incr_DATA_READ(){
        DATA_READ++;
    }

    public static String display(){
        return "\n\n\tMetrics values :"+
                "\nPROG_SIZE : "+PROG_SIZE+" intructions"+
                "\nEXEC_TIME : "+EXEC_TIME+ " ms"+
                "\nEXEC_MOVE : "+EXEC_MOVE+" intructions executed"+
                "\nDATA_MOVE : "+DATA_MOVE+" times moving memory pointer"+
                "\nDATA_WRITE : "+DATA_WRITE+" times writing the memory"+
                "\nDATA_READ : "+DATA_READ+" times accessing a value in memory"+"\n";
    }
}
