package mkdercode.tests_package;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

import mkdercode.Constante.Config;
import mkdercode.Exception.BnfkEndOfFile;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkOutOfDiskException;
import mkdercode.Exception.BnfkOverFlowException;
import mkdercode.Model.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * CLASSE :
 *
 * @author mxm
 *         07/11/16.
 */
public class CommandTest {
    Memory m;

    @Test
    public void test_increment() throws BnfkException {
        m = new Memory();
        CommandIncr incr = new CommandIncr(m);
        Command commands[] = {incr};
        for (Command c : commands) c.exec();
        Assert.assertEquals(1, m.getCopyValueOf(0));
    }

    @Test
    public void test_decrement() throws BnfkException {
        m = new Memory();
        CommandIncr incr = new CommandIncr(m);
        CommandDecr decr = new CommandDecr(m);
        Command commands[] = {incr, decr};
        for (Command c : commands) c.exec();
        Assert.assertEquals(0, m.getCopyValueOf(0));
    }

    @Test
    public void test_pointerRight() throws BnfkException {
        m = new Memory();
        CommandRight right = new CommandRight(m);
        CommandIncr incr = new CommandIncr(m);

        Command commands[] = {right, incr};
        for (Command c : commands) c.exec();
        Assert.assertEquals(1, m.getCopyValueOf(1));
    }

    @Test
    public void test_pointerLeft() throws BnfkException {
        m = new Memory();
        Command right = new CommandRight(m);
        CommandLeft left = new CommandLeft(m);
        CommandIncr incr = new CommandIncr(m);

        Command commands[] = {right, left, incr, incr};
        for (Command c : commands) c.exec();
        Assert.assertEquals(2, m.getCopyValueOf(0));
    }

    //cas d'erreur
    @Test(expected = BnfkOverFlowException.class)
    public void test_increment_err() throws BnfkException {
        m = new Memory();
        CommandIncr incr = new CommandIncr(m);

        for (int i = 0; i <= Config.MAX_NUMBER; i++)
            incr.exec();

    }

    @Test(expected = BnfkOverFlowException.class)
    public void test_decrement_err() throws BnfkException {
        m = new Memory();

        CommandDecr decr = new CommandDecr(m);
        decr.exec();
    }

    @Test(expected = BnfkOutOfDiskException.class)
    public void test_pointerRight_err() throws BnfkException {
        m = new Memory();
        CommandRight right = new CommandRight(m);
        for (int i = 0; i <= Config.MEMORY_SIZE; i++)
            right.exec();

    }

    @Test(expected = BnfkOutOfDiskException.class)
    public void test_pointerLeft_err() throws BnfkException {
        m = new Memory();
        CommandLeft left = new CommandLeft(m);
        left.exec();

    }

    @Test
    public void testIn() throws BnfkException, UnsupportedEncodingException {
        m = new Memory();
        InputStream is = new ByteArrayInputStream(("data").getBytes("UTF-8"));
        Config.PATH_IN = is;
        Command in = new CommandIn(m);
        Command right = new CommandRight(m);
        Command commands[] = {in, right, in};
        for (Command c : commands) c.exec();
        Assert.assertEquals(m.getVal(0), 'd');
        Assert.assertEquals(m.getVal(1), 'a');
    }

    @Test(expected = BnfkEndOfFile.class)
    public void testIn_err() throws UnsupportedEncodingException, BnfkException {
        m = new Memory();
        InputStream is = new ByteArrayInputStream(("").getBytes("UTF-8"));
        Config.PATH_IN = is;
        Command in = new CommandIn(m);
        in.exec();
    }

    @Test
    public void testOut() throws BnfkException, UnsupportedEncodingException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        m = new Memory();
        CommandIncr incr = new CommandIncr(m);
        CommandOut out = new CommandOut(m);
        Command commands[] = {incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr,
                incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr, incr,
                out};
        Config.PATH_OUT = System.out;
        for (Command c : commands) c.exec();
        Assert.assertEquals("!", outContent.toString());
    }

}