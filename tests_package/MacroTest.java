/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import java.util.ArrayList;
import mkdercode.Entree.Macro;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class MacroTest {
    
    private static Macro ma;
    private static String name;
    private  static ArrayList<String> body;
    
    public MacroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        name = "mymac";
        body = new ArrayList<>();
        body.add("++++");
        ma = new Macro(name, body);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void test_getName() {
        assertEquals(ma.getName(), name);
    }
    
    @Test
    public void test_getBody() {
        assertEquals(ma.getBody(), body);
    }
}
