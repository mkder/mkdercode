/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkBackInfinite;
import mkdercode.Exception.BnfkEndOfFile;
import mkdercode.Exception.BnfkException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class BnfkBackInfiniteTest {
    private BnfkBackInfinite b;

    @Before
    public void BnfkBackInfiniteTest() {
        b = new BnfkBackInfinite();
    }

    @Test
    public void testSomeMethod() {
        assertEquals(b.getMessage(),"La commande back a été appelée sans command jump associée");
    }
    
    @Test
    public void testgetexitcode() {
        assertEquals(b.getExitCode(),4);
    }
}
