package mkdercode.tests_package;

import mkdercode.Constante.Directives;
import mkdercode.Entree.CodeReader;
import mkdercode.Exception.BnfkException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * CLASSE :
 *
 * @author mxm
 *         06/11/16.
 */
public class CodeReaderTest{
    private String testCode  = "++++[>+++++<-]"; //A mettre dans le fichier de test
    private String path = "mkder_bnfk_test_files_code_reader.txt"; //nom du fichier de test
    private List<Directives> listDirectiveTest;

    @Test
    public void testGetNextDirectives() throws BnfkException{
        File fileTest = new File(path);
        try{
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }
        CodeReader reader = new CodeReader(path);
        listDirectiveTest = new ArrayList<>();
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.JUMP);
        listDirectiveTest.add(Directives.RIGHT);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.LEFT);
        listDirectiveTest.add(Directives.DECR);
        listDirectiveTest.add(Directives.BACK);
        listDirectiveTest.add(null);

        for (Directives directives : listDirectiveTest){
            Assert.assertEquals(directives, reader.getNextDirective());
        }

        fileTest.delete();
        new File(path + ".out").delete();
    }
}
