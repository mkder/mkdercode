/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkEndOfFile;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkFileNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class BnfkFreeBlockExceptionTest {

    private BnfkFileNotFoundException b;

    @Before
    public void BnfkFreeBlockExceptionTest() {
        b = new BnfkFileNotFoundException("path");
    }

    @Test
    public void testSomeMethod() {
        assertEquals(b.getMessage(),"BnfkFileNotFoundException : Le fichier path est introuvable ou ne peut pas être modifié.");
    }
    
}
