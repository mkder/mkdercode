package mkdercode.tests_package;

import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkOutOfDiskException;
import mkdercode.Model.Memory;
import org.junit.*;

import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class MemoryTest {
    private static Memory mm;
    
    public MemoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws BnfkOutOfDiskException, BnfkException {
        mm = new Memory();
    }
    
    @Test
    public void testToString() {
        String res = mm.toString();
        assertEquals("",res);
    }
    
    @Test
    public void testgetPointerVal() {
        assertEquals(mm.getPointerVal(), 0);
    }
    
    @Test
    public void testsetPointer() throws BnfkOutOfDiskException {
        int ptr = 8;
        mm.setPointer(ptr);
        assertEquals(mm.getPointerVal(), 8);
    }
    
    @Test
    public void testgetAbsolutePointer() throws BnfkOutOfDiskException {
        int ptr = 8;
        assertEquals(mm.getAbsolutePointer(ptr), 8);
    }
    
    @Test
    public void testgetVal() throws BnfkException {
        int ptr = 2; short val = 5;
        mm.setPointer(ptr);
        mm.setCurrentValue(val);
        mm.setPointer(0);
        assertEquals(val, mm.getVal(ptr));
    }
    
    @Test
    public void testgetCurrentVal() throws BnfkException {
        int ptr = 2; short val = 5;
        mm.setCurrentValue(val);
        assertEquals(val, mm.getCurrentValue());
    }
    
    @Test
    public void testsetCurrentVal() throws BnfkException {
        int ptr = 2; short val = 5;
        mm.setCurrentValue(val);
        assertEquals(val, mm.getCurrentValue());
    }
    
    @Test
    public void testsetCurrentValues() throws BnfkException {
        short val[] = {0, 1, 2, 3, 4, 5};
        mm.setCurrentValues(val);
        for(int i =0; i<5; i++)
            assertEquals(val[i], mm.getVal(i));
    }
    
    @Test
    public void testgetCopyValueOf() throws BnfkException {
        int ptr = 2;
        short val = 5;
        mm.setPointer(ptr);
        mm.setCurrentValue(val);
        //mm.getCopyValueOf(ptr);
        assertEquals(val, mm.getCopyValueOf(ptr));
        assertEquals(-1, mm.getCopyValueOf(-1));
        assertEquals(-1, mm.getCopyValueOf(30001));
    }
    
    @Test
    public void test_allocMemory() throws BnfkException {
        mm.setCurrentValue((short)5);
        Assert.assertEquals(5, mm.getCurrentValue());
        mm.allocMemory();
        Assert.assertEquals(0, mm.getCurrentValue());
    }

    @Test
    public void test_deAllocMemory() throws BnfkException {
        mm.setCurrentValue((short)3);
        mm.allocMemory();
        mm.setCurrentValue((short)8);
        Assert.assertEquals(8, mm.getCurrentValue());
        mm.deAllocMemory();
        Assert.assertEquals(3, mm.getCurrentValue());
    }
}
