/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkEndOfFile;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkFreeBlockException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class BnfkEndOfFileTest {
    private BnfkException b;

    @Before
    public void BnfkEndOfFileTest() {
        b = new BnfkEndOfFile("test");
    }

    @Test
    public void testSomeMethod() {
        assertEquals(b.getMessage(),"la fonction " + "test" + " a atteind le bout du fichier.");
    }
    
}
