package mkdercode.tests_package;

import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkException;
import mkdercode.Model.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Elie on 24/11/2016.
 */
public class InstructionsStackTest {
    private String testCode  = "++++[>+++++<-]+"; //A mettre dans le fichier de test
    private String path = "mkder_bnfk_test_files_stack.txt"; //nom du fichier de test
    private List<Directives> listDirectiveTest;

    private String[] listDirectiveProcTest =
            new String[]{"@function f1 0 :", "++", "@return", "@main", "++-", "f1"};

    @Test
    public void test_getNext() throws BnfkException{
        File fileTest = new File(path);
        try{
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }

        InstructionsStack stack = new InstructionsStack(path);
        listDirectiveTest = new ArrayList<>();
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.JUMP);
        listDirectiveTest.add(Directives.RIGHT);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.LEFT);
        listDirectiveTest.add(Directives.DECR);
        listDirectiveTest.add(Directives.BACK);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(null);

        for(Directives directives : listDirectiveTest){
            Assert.assertEquals(directives, stack.getNext());
        }

        fileTest.delete();
        new File(path + ".out").delete();
    }

    @Test
    public void test_jump_back() throws BnfkException{
        File fileTest = new File(path);
        try{
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }

        //Test pour JUMP
        InstructionsStack stack = new InstructionsStack(path);
        listDirectiveTest = new ArrayList<>();
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.JUMP);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(null);

        for(Directives directives : listDirectiveTest){
            Assert.assertEquals(directives, stack.getNext());
            if(directives == Directives.JUMP)
                stack.jumpTo();
        }

        //Test pour BACK une fois
        InstructionsStack stack1 = new InstructionsStack(path);
        listDirectiveTest.clear();
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.JUMP);
        listDirectiveTest.add(Directives.RIGHT);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.LEFT);
        listDirectiveTest.add(Directives.DECR);
        listDirectiveTest.add(Directives.BACK);
        listDirectiveTest.add(Directives.RIGHT);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.LEFT);
        listDirectiveTest.add(Directives.DECR);
        listDirectiveTest.add(Directives.BACK);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(null);

        boolean oneBack = false;
        for(Directives directives : listDirectiveTest){
            Assert.assertEquals(directives, stack1.getNext());
            if(directives == Directives.BACK && !oneBack){
                stack1.jumpTo();
                oneBack = true;
            }
        }

        fileTest.delete();
        new File(path + ".out").delete();
    }

    @Test
    public void test_procedure() throws BnfkException{
        File fileTest = new File(path);
        try{
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(fileTest)));
            for(String instr : listDirectiveProcTest){
                writer.println(instr);
            }
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }

        InstructionsStack stack = new InstructionsStack(path);
        listDirectiveTest = new ArrayList<>();
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.DECR);
        listDirectiveTest.add(Directives.PROCEDURE_CALL);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.INCR);
        listDirectiveTest.add(Directives.RETURN);
        listDirectiveTest.add(null);

        for(Directives directives : listDirectiveTest){
            Assert.assertEquals(directives, stack.getNext());
            if(directives == Directives.PROCEDURE_CALL)
                stack.callProcedure();
            if(directives == Directives.RETURN)
                stack.returnProcedure();
        }

        fileTest.delete();
        new File(path + ".out").delete();
    }
}
