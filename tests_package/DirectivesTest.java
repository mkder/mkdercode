/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Constante.Directives;
import static mkdercode.Constante.Directives.*;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class DirectivesTest {
    //private static String dir="INCR "+ "DECR "+ "BACK "+
        //"LEFT "+ "RIGHT "+ "JUMP ";
    private static Directives direc;
    private String[] strShort ={"+", "-", "<", ">", ",", ".", "[", "]", null};
    private String[] strLong = {"INCR", "DECR", "LEFT", "RIGHT", "IN", "OUT", "JUMP", "BACK", null};
    private String[] strRGB = {"#FFFFFF", "#4B0082", "#9400D3", "#0000FF", "#FFFF00", "#00FF00", "#FF7F00", "#FF0000", null};
    private Directives[] dir = {Directives.INCR, Directives.DECR, Directives.LEFT,
            Directives.RIGHT, Directives.IN, Directives.OUT, Directives.JUMP, Directives.BACK, Directives.VOID};
    public DirectivesTest() {
    }

    @Test
    public void testtoDirectives() {
        assertEquals(toDirective("+"), INCR);
        assertEquals(toDirective("-"), DECR);
        assertEquals(toDirective("DECR"), DECR);
    }
    
    @Test
    public void exists() {
        String[] dir ={"+", "-", "<", ">"};
        for (String dir1 : dir) {
            assertTrue(Directives.exists(dir1));
        }
    }
    
    @Test
    public void testisTheDirective() {
        for(int i = 0 ; i < strLong.length - 1; i++){
            assertTrue(dir[i].isTheDirective(strLong[i]));
        }
    }
    
    @Test
    public void testgetLong() {
        for(int i = 0 ; i < strLong.length; i++){
            assertEquals(dir[i].getLong(), strLong[i]);
        }
    }

    @Test
    public void test_getShort(){
        for(int i = 0 ; i < strShort.length; i++){
            assertEquals(dir[i].getShort(), strShort[i]);
        }
    }

    @Test
    public void test_getRGB(){
        for(int i = 0 ; i < strRGB.length; i++){
            assertEquals(dir[i].getRGB(), strRGB[i]);
        }
    }
}
