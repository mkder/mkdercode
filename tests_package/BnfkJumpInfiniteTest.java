/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkJumpInfinite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author user
 */
public class BnfkJumpInfiniteTest {

    private BnfkJumpInfinite b;

    @Before
    public void BnfkJumpInfiniteTest() {
        b = new BnfkJumpInfinite();
    }

    @Test
    public void testSomeMethod() {
        assertEquals(b.getMessage(),"La commande JUMP a été appelé mais aucune commande BACK ne lui est associée.");
    }
    
    @Test
    public void testgetexitcode() {
        assertEquals(b.getExitCode(),4);
    }
}
