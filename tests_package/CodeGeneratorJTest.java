package mkdercode.tests_package;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import mkdercode.CodeGenerator.CodeGeneratorJ;
import mkdercode.Exception.BnfkException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class CodeGeneratorJTest {

    private static String testCode = "++++[>+++++<-]"; 
    private static String path = "mkder_bnfk_test_files_interpretor.txt";
    private static String res = "import java.util.*;\n" +
                                "public class Main {\n" +
                                "public static void main(String argv[]){\n" +
                                "Scanner sc = new Scanner(System.in);\n" +
                                "int t[] = new int[30000];\n" +
                                "int ptr = 0;\n" +
                                "for(int i = 0; i < 30000; i++){t[i] = 0;}\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "while(t[ptr]>0){\n" +
                                "ptr++;\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "t[ptr]++;\n" +
                                "ptr--;\n" +
                                "t[ptr]--;\n" +
                                "}\n" +
                                "}\n" +
                                "}";
    private static File fileTest;
    private static CodeGeneratorJ cg;

    public CodeGeneratorJTest() {
    }

    @BeforeClass
    public static void setUpClass() throws BnfkException {
        fileTest = new File(path);
        try {
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }
        cg = new CodeGeneratorJ(path);
    }

    @AfterClass
    public static void tearDownClass() {
        new File(path).delete();
        new File("mkder_bnfk_test_files_interpretor.java").delete();
    }

    @Test
    public void exec() throws BnfkException, FileNotFoundException {
        cg.exec();
        InputStream is = new FileInputStream(new File("mkder_bnfk_test_files_interpretor.java"));
        String result = new BufferedReader(new InputStreamReader(is))
                .lines().collect(Collectors.joining("\n"));
        assertEquals(result, res);
    }
}
