package mkdercode.tests_package;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Elie on 13/01/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CheckerTest.class,
        CodeGeneratorCTest.class,
        CodeReaderTest.class,
        //CommandTest.class,
        DirectivesTest.class,
        ImageManagerTest.class,
        InstructionsStackTest.class,
        LineParserTest.class,
        //MacroReader.class,
        MacroTest.class,
        MemoryTest.class,
        //ProcedureReader.class,
        ProcedureTest.class,
        //RewriterTest.class,
        SuperBlockTest.class,
        VMTest.class
}
)
public class JunitTestSuite {
}
