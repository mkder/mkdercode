/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkSyntaxException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class BnfkSyntaxExceptionTest {
    
    private static BnfkSyntaxException error, error1, error2, error3;
    private static int line = 3;
    private static String cause = "123";
    
    public BnfkSyntaxExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        error = new BnfkSyntaxException();
        error1 = new BnfkSyntaxException(line);
        error2 = new BnfkSyntaxException(cause);
        error3 = new BnfkSyntaxException(line, cause);
    }
    

    @Test
    public void testGetMessage() {
        assertEquals(error.getMessage(), "Caractere incorrect.");
        assertEquals(error1.getMessage(), "Caractère incorrect a la ligne ligne :"+line);
        assertEquals(error2.getMessage(), cause);
        assertEquals(error3.getMessage(), "Erreur de syntaxe à la ligne " + line + " : " + cause);
        
    }
}
