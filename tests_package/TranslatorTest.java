/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import static junit.framework.TestCase.assertEquals;
import mkdercode.Constante.Config;
import mkdercode.Constante.Directives;
import mkdercode.Controler.Translator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import mkdercode.Constante.Directives.*;

/**
 *
 * @author rash
 */
public class TranslatorTest {
    private static String testCode  = "+-><[]"; //A mettre dans le fichier de test
    private static String path = "mkder_bnfk_test_files_interpretor.txt"; //nom du fichier de test
    private static List<Directives> listDirectiveTest;
    private static File fileTest = new File(path);
    private static Translator tr;
    private static BufferedImage imgTr;
    private static int h, w, h1, w1;
    private static String Trpath = "mkder_bnfk_test_files_interpretor.bmp";
    private static List dir;
    

    
    public TranslatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        try{
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }
        tr = new Translator(path);
        tr.exec();
        imgTr = ImageIO.read(new File(Trpath));
        dir = new ArrayList<Directives>();
        
        dir.add(Directives.INCR);
        dir.add(Directives.DECR);
        dir.add(Directives.RIGHT);
        dir.add(Directives.LEFT);
        dir.add(Directives.JUMP);
        dir.add(Directives.BACK);
    }
    
    @AfterClass
    public static void tearDownClass() {
        fileTest.delete();
        new File(path + ".out").delete();
        new File(Trpath).delete();
    }

    @Test
    public void testImage() {
        int x=0, y=0, cnt = 0;
        int width = imgTr.getWidth(), height = imgTr.getHeight();
        Color pixelValue;
        while ( x < width && y < height) {
            pixelValue = new Color(imgTr.getRGB(x, y));
            assertEquals(Directives.toDirective(toRGB(pixelValue).toUpperCase()), dir.get(cnt));
            if (x == width - Config.INSTR_SIZE) y += Config.INSTR_SIZE;
            if (x < width - Config.INSTR_SIZE)
                x += Config.INSTR_SIZE;
            else x = 0;
            cnt++;
        }
    }
    
    private String toRGB(Color color) {
        return "#" + Integer.toHexString(color.getRGB()).substring(2);
    }
    

}
