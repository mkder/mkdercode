package mkdercode.tests_package;

import mkdercode.Constante.Directives;
import mkdercode.Entree.ImageManager;
import mkdercode.Exception.BnfkException;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

import static mkdercode.Constante.Directives.*;

public class ImageManagerTest{

    private String testCode = "+++--[+++].,,";
    private String path = "mkder_bnfk_test_files_image_manager.bmp";
    private ArrayList<Directives> listDirectiveTest;

    @Test
    public void testTranslateAndReadeImage() throws BnfkException{
        listDirectiveTest = new ArrayList<>();
        listDirectiveTest.add(INCR);
        listDirectiveTest.add(INCR);
        listDirectiveTest.add(INCR);
        listDirectiveTest.add(DECR);
        listDirectiveTest.add(DECR);
        listDirectiveTest.add(JUMP);
        listDirectiveTest.add(INCR);
        listDirectiveTest.add(INCR);
        listDirectiveTest.add(INCR);
        listDirectiveTest.add(BACK);
        listDirectiveTest.add(OUT);
        listDirectiveTest.add(IN);
        listDirectiveTest.add(IN);

        //Traduction list -> image
        ImageManager imageManager = new ImageManager(path);
        imageManager.translate(listDirectiveTest, listDirectiveTest.size());
        imageManager.display();

        //Traduction image -> list
        ImageManager imageManager1 = new ImageManager(path);
        for(int i = 0; i < listDirectiveTest.size(); i++){
            Assert.assertEquals(imageManager1.getNextDirective(), listDirectiveTest.get(i));
        }

        new File(path).delete();
    }
}