/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkOutOfDiskException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class BnfkOutOfDiskExceptionTest {
    private static String moreInfo = "info";
    private static BnfkOutOfDiskException error;
    
    public BnfkOutOfDiskExceptionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        error = new BnfkOutOfDiskException(moreInfo);
    }
    

    @Test
    public void testGetMessage() {
        assertEquals(error.getMessage(), "MemoryException : BnfkOutOfDiskException : Le pointeur ne peut pas se deplacer a l'exterieur de la memoire - "+moreInfo);
    }
    
    @Test
    public void testgetexitcode() {
        assertEquals(error.getExitCode(),2);
    }
}
