package mkdercode.tests_package;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;
import mkdercode.CodeGenerator.CodeGeneratorC;
import mkdercode.Exception.BnfkException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class CodeGeneratorCTest {

    private static String testCode = "++++[>+++++<-]"; 
    private static String path = "mkder_bnfk_test_files_interpretor.txt";
    private static String res = "#include <stdlib.h>\n"
            + "#include <stdio.h>\n"
            + "int main() {\n"
            + "int *t = malloc(sizeof(int) * 30000);\n"
            + "for(int i = 0; i < 30000; i++)\n"
            + "t[i] = 0;\n"
            + "int *ptr = t;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "while(*ptr){\n"
            + "ptr++;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "(*ptr)++;\n"
            + "ptr--;\n"
            + "(*ptr)--;\n"
            + "}\n"
            + "return 0;\n"
            + "}";
    private static File fileTest;
    private static CodeGeneratorC cg;

    public CodeGeneratorCTest() {
    }

    @BeforeClass
    public static void setUpClass() throws BnfkException {
        fileTest = new File(path);
        try {
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }
        cg = new CodeGeneratorC(path);
    }

    @AfterClass
    public static void tearDownClass() {
        new File(path).delete();
        new File("mkder_bnfk_test_files_interpretor.c").delete();
    }

    @Test
    public void exec() throws BnfkException, FileNotFoundException {
        cg.exec();
        InputStream is = new FileInputStream(new File("mkder_bnfk_test_files_interpretor.c"));
        String result = new BufferedReader(new InputStreamReader(is))
                .lines().collect(Collectors.joining("\n"));
        assertEquals(result, res);
    }
}
