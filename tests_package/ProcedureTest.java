package mkdercode.tests_package;

import mkdercode.Entree.Procedure;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class ProcedureTest {
    
    private static Procedure proc, proc2;
    private static String name = "myproc"; 
    private static int nbParametre = 3;
    private static int[] valParametre = {1, 2, 3};
    
    @BeforeClass
    public static void setUpClass() {
        proc = new Procedure(name, valParametre);
        proc2 = new Procedure(name, nbParametre);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void testgetName() {
        assertEquals(proc.getName(), name);
    }
    
    @Test
    public void testNbParametre() {
        assertEquals(proc2.getNbParametre(), nbParametre);
    }
    @Test
    public void testValParametre() {
        for(int i=0; i<nbParametre; i++){
            assertEquals(proc.getValParametre()[i], valParametre[i]);
        }
    }
}
