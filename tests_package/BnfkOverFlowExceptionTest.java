/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mkdercode.tests_package;

import mkdercode.Exception.BnfkOverFlowException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class BnfkOverFlowExceptionTest {
    
    private static BnfkOverFlowException error;
    
    public BnfkOverFlowExceptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        error = new BnfkOverFlowException();
    }
    

    @Test
    public void testGetMessage() {
        assertEquals(error.getMessage(), "MemoryException : Depassement de la capacité de représentation de la mémoire: nombre < 0 / nombre > 255");
    }
    
    @Test
    public void testgetexitcode() {
        assertEquals(error.getExitCode(),1);
    }
}
