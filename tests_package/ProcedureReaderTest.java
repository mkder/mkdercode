package mkdercode.tests_package;

import java.util.ArrayList;
import java.util.List;
import mkdercode.Entree.Procedure;
import mkdercode.Entree.ProcedureReader;
import mkdercode.Exception.BnfkSyntaxException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class ProcedureReaderTest {
    
    private static ProcedureReader procr;
    private static Procedure proc;
    
    @BeforeClass
    public static void setUpClass() {
        proc = new Procedure("myproc", 3);
        procr = new ProcedureReader();   
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Test
    public void testget() {
        procr.put(proc);
        assertEquals(procr.get("myproc"), proc);
    }
    
    @Test
    public void testisProcedure() {
        procr.put(proc);
        assertTrue(procr.isProcedure("myproc"));
    }

    @Test
    public void test_readProcedure() throws BnfkSyntaxException{
        procr.put(proc);
        List<String> line = new ArrayList<>();
        line.add("myproc");
        line.add("0");
        line.add("1");
        line.add("2");
        Procedure procedure = procr.readProcedure(line);
        int[] valParametre = procedure.getValParametre();
        for(int i = 0; i < 3; i++){
            Assert.assertEquals(i, valParametre[i]);
        }
    }
}
