package mkdercode.tests_package;

import java.util.ArrayList;
import java.util.List;
import mkdercode.Entree.Macro;
import mkdercode.Entree.MacroReader;
import mkdercode.Exception.BnfkSyntaxException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rash
 */
public class MacroReaderTest {
    
    private static MacroReader macroReader;
    private static Macro macro;
    private static String name = "mymac";
    private static String body = "++++";
    
    @BeforeClass
    public static void setUpClass() {
        macroReader = new MacroReader();
        ArrayList<String> a = new ArrayList<>();
        a.add(body);
        macro = new Macro(name, a);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Test
    public void test_get() {
        macroReader.put(macro);
        assertEquals(macroReader.get(name), macro);
    }
    
    @Test
    public void test_isMacro() {
        macroReader.put(macro);
        assertTrue(macroReader.isMacro(name));
        assertTrue(!macroReader.isMacro("mac"));
    }

    @Test
    public void test_readMacro() throws BnfkSyntaxException{
        macroReader.put(macro);
        List<String> line = new ArrayList<>();
        ArrayList<String> macroBody = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            macroBody.add("++++");
        }
        line.add("mymac");
        line.add("5");

        ArrayList<String> macroBody2 = macroReader.readMacro(line);
        for(int i = 0; i < macroBody.size(); i++){
            Assert.assertEquals(macroBody.get(i), macroBody2.get(i));
        }
    }
}
