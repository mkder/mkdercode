package mkdercode.tests_package;

import mkdercode.Controler.Rewriter;
import mkdercode.Exception.BnfkException;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class RewriterTest {
    private String testCode = "++++[>+++++<-]";
    private String path = "mkder_bnfk_test_files_rewriter.txt";
    private String[] listDirectiveTest =
            new String[]{"INCR", "INCR", "INCR", "INCR", "JUMP", "RIGHT",
                    "INCR", "INCR", "INCR", "INCR", "INCR", "LEFT", "DECR", "BACK"};

    @Test
    public void testexec() throws BnfkException {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        File fileTest = new File(path);
        try {
            PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(fileTest)));
            for (String instr : listDirectiveTest) {
                writer.println(instr);
            }
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        Rewriter rewriter = new Rewriter(path);
        rewriter.exec();
        Assert.assertEquals(testCode, outContent.toString());

        fileTest.delete();
        new File(path + ".out").delete();
    }
}