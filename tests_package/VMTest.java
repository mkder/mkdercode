package mkdercode.tests_package;

import mkdercode.Controler.VM;
import mkdercode.Exception.BnfkFileNotFoundException;
import mkdercode.Exception.BnfkWrongArgException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class VMTest {
    private static String testCode  = "++++[>+++++<-]"; //A mettre dans le fichier de test
    private static String path = "mkder_bnfk_test_files_vm.txt"; //nom du fichier de test
    private String pathImage = "mkder_bnfk_test_files_vm.bmp";
    private static File fileTest;

    @BeforeClass
    public static void before_test_vm(){
        fileTest = new File(path);
        try{
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    @AfterClass
    public static void after_test_vm(){
        fileTest.delete();
        new File(path + ".out").delete();
    }

    @Test
    public void testexecArgRead() throws BnfkFileNotFoundException, BnfkWrongArgException {
        String args[] = {"-p" , path};
        VM vm = new VM();
        vm.execute(args);
        Assert.assertEquals(vm.getExeType(),"Interpretor");
        //vm.stop();
    }

    @Test
    public void testexecArgCheck(){
        String args[] = {"-p" , path, "--check"};
        VM vm = new VM();
        vm.execute(args);
        Assert.assertEquals(vm.getExeType(),"Checker");
    }

    @Test
    public void testexecArgRewrite(){
        String args[] = {"--rewrite", "-p" , path};
        VM vm = new VM();
        vm.execute(args);
        Assert.assertEquals(vm.getExeType(),"Rewriter");
    }

    @Test
    public void testexecArgTranslateAndReadImage(){
        String args[] = {"--translate", "-p" , path};
        VM vm = new VM();
        vm.execute(args);
        Assert.assertEquals(vm.getExeType(),"Translator");

        String args1[] = {"-p" , pathImage};
        VM vm1 = new VM();
        vm1.execute(args1);
        Assert.assertEquals(vm1.getExeType(),"Interpretor");
        new File(pathImage).delete();
    }

    @Test
    public void testexecArgLog() throws BnfkFileNotFoundException, BnfkWrongArgException {
        String args[] = {"--trace", "-p" , path};
        VM vm = new VM();
        vm.execute(args);
        Assert.assertEquals(vm.getExeType(),"Interpretor");
    }
}