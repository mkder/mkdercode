package mkdercode.tests_package;

import mkdercode.Controler.Checker;
import mkdercode.Exception.BnfkJumpInfinite;
import org.junit.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * CLASSE :
 *
 * @author mxm
 *         16/11/16.
 */
public class CheckerTest {
    Checker check;
    private static String validPath = "mkder_bnfk_test_file_checker_true.txt";
    private static String wrongPath = "mkder_bnfk_test_file_checker_false.txt";
    private static String testCode1 = "+++[>+<-]";
    private static String testCode2 = "++[>+[>++<]<-][";

    @BeforeClass
    public static void addFile(){
        File fileTest = new File(validPath);
        try {
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode1.getBytes(StandardCharsets.UTF_8));
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        File fileTest2 = new File(wrongPath);
        try {
            FileOutputStream writer = new FileOutputStream(fileTest2);
            writer.write(testCode2.getBytes(StandardCharsets.UTF_8));
            writer.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }
    }

    @AfterClass
    public static void removeFile(){
        new File(validPath).delete();
        new File(validPath + ".out").delete();
        new File(wrongPath).delete();
        new File(wrongPath + ".out").delete();
    }

    @Test
    public void testexecValid() throws Exception {
        check = new Checker(validPath);
        check.exec();
        Assert.assertEquals(true, check.isValid());
    }

    @Test(expected = BnfkJumpInfinite.class)
    public void testexecInvalid() throws Exception {
        check = new Checker(wrongPath);
        check.exec();
    }
}