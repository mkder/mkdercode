package mkdercode.tests_package;

import mkdercode.Model.SuperBlock;
import org.junit.Assert;
import org.junit.Test;


public class SuperBlockTest {
    @Test
    public void testupdateBlock() {
        SuperBlock sb = new SuperBlock();
        boolean un = sb.updateBlock(30000);
        boolean deux = sb.updateBlock(-30000);
        boolean trois = sb.updateBlock(150);

        Assert.assertTrue(trois);
        Assert.assertFalse(deux);
        Assert.assertFalse(un);
    }

    @Test
    public void testisFreeBlock() throws Exception {
        SuperBlock sb = new SuperBlock();
        Assert.assertFalse(sb.isFreeBlock(-1));
        Assert.assertFalse(sb.isFreeBlock(30000));
        Assert.assertTrue(sb.isFreeBlock(29999));
        Assert.assertTrue(sb.isFreeBlock(0));
        sb.updateBlock(0);
        Assert.assertFalse(sb.isFreeBlock(0));
    }

}