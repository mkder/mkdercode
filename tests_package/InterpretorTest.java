package mkdercode.tests_package;

import junit.framework.TestCase;
import mkdercode.Constante.Directives;
import mkdercode.Controler.Interpretor;
import org.junit.Assert;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//TODO: le le fait de plus exec la VM rends les tests plus difficiles
public class InterpretorTest extends TestCase{
    private String testCode  = "++++[>+++++<-]"; //A mettre dans le fichier de test
    private String path = "mkder_bnfk_test_files_interpretor.txt"; //nom du fichier de test
    private List<Directives> listDirectiveTest;
    private String res =    "[c0]: 0\n" +
                            "[c1]: 20\n\n";
    private String[] res2 = {"[c0]: 0", "[c1]: 20"};

    public void test_exec() throws Exception {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        File fileTest = new File(path);
        try{
            FileOutputStream writer = new FileOutputStream(fileTest);
            writer.write(testCode.getBytes(StandardCharsets.UTF_8));
            writer.close();
        }catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }

        Interpretor interpretor = new Interpretor(path);
        interpretor.exec();

        Scanner scanner = new Scanner(outContent.toString());
        List<String> lines = new ArrayList<>();
        while (scanner.hasNext()){
            lines.add(scanner.nextLine());
        }
        scanner.close();
        for(int i = 0; i < res2.length; i++){
            Assert.assertEquals(res2[i], lines.get(i));
        }

        fileTest.delete();
        new File(path + ".out").delete();
    }
}