package mkdercode.tests_package;

import mkdercode.Constante.Directives;
import mkdercode.Entree.LineParser;
import mkdercode.Exception.BnfkSyntaxException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author user
 */
public class LineParserTest {
    private String line = "+-<>,.[]";
    private Directives[] dir = {Directives.INCR, Directives.DECR, Directives.LEFT,
    Directives.RIGHT, Directives.IN, Directives.OUT, Directives.JUMP, Directives.BACK};

    @Test
    public void test_parse() throws BnfkSyntaxException{
        LineParser parser = new LineParser();
        parser.parse(line);
        for(int i = 0; i < dir.length; i++){
            Assert.assertEquals(dir[i], parser.getNextDirective());
        }
    }

    @Test
    public void test_find() throws BnfkSyntaxException {
        LineParser parser = new LineParser();
        parser.parse(line);
        Assert.assertTrue(parser.find());
    }
}
