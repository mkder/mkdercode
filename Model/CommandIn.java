package mkdercode.Model;

import mkdercode.Constante.Config;
import mkdercode.Exception.BnfkEndOfFile;
import mkdercode.Exception.BnfkException;

import java.io.IOException;

/**
 * @author Rachida
 */
public class CommandIn extends Command {
    public CommandIn(MemoryInterface m) {
        super(m);
    }

    public void exec() throws BnfkException {
        getChar();
    }

    private void getChar() throws BnfkException {
        try {
            if (Config.PATH_IN == System.in) {
                int c;
                char inValue;
                do {
                    System.out.print("Saisissez le caractère pour la variable [c" + getMemory().getPointerVal() + "]: ");
                    c = Config.PATH_IN.read();
                    cleanBuffer();
                } while (c > Config.MAX_NUMBER || c < 0);
                inValue = (char) c;
                getMemory().setCurrentValue((short) inValue);

            } else {
                int c = Config.PATH_IN.read();
                if (c == -1) {
                    throw new BnfkEndOfFile("IN");
                }
                char inValue = (char) c;
                getMemory().setCurrentValue((short) inValue);
            }
        }catch (IOException e){
            throw new BnfkException(e.getMessage());
        }
    }

    //Le C c'est bien
    private void cleanBuffer() throws IOException{
        while ((Config.PATH_IN.read()) != '\n');
    }
}
