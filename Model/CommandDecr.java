package mkdercode.Model;

import mkdercode.Exception.BnfkException;

/**
 * CLASSE :
 *
 * @author mxm
 *         27/10/16.
 */
public class CommandDecr extends Command {
    public CommandDecr(MemoryInterface m){
        super(m);
    }

    @Override
    public void exec() throws BnfkException {
        decrementCurrentValue();
    }

    private void decrementCurrentValue() throws BnfkException {
        getMemory().setCurrentValue((short) (getMemory().getCurrentValue() - 1));
    }
}
