package mkdercode.Model;

import mkdercode.Constante.Config;

public class SuperBlock implements SuperBlockInterface{

	private boolean[] bitmap;

	/**
	 * Cette methode passe a true la case mémoire ciblee dans la bitmap
	 * Un block utilise a une valeur true
	 * Un block vide a une valeur false
	 * @param blockNumber
	 */
	public boolean updateBlock(int blockNumber) {
		//Controle de dépassement
		if(blockNumber> Config.MEMORY_SIZE -1 || blockNumber < 0){
			return false;
			//ca correspondra a un dépassement : renvoye dans la classe memoire
		}
		if(!bitmap[blockNumber]){
			bitmap[blockNumber] = true;
		}
		return bitmap[blockNumber];

	}

	/**
	 * isFreeBlock dit si un bloc est libre
	 * @return true si le bloc est libre, false s'il a deja ete alloue dans le programme
	 * @param blockNumber : int
	 */
	public boolean isFreeBlock(int blockNumber) {

        if(blockNumber> Config.MEMORY_SIZE -1 || blockNumber < 0){
            return false;
            //ca correspondra a un dépassement : renvoye dans la classe memoire
        }return !bitmap[blockNumber];
	}

	//Libere une case memoire
	public boolean freeBlock(int blockNumber){
        if(blockNumber > Config.MEMORY_SIZE - 1 || blockNumber < 0){
            return false;
        }
        bitmap[blockNumber] = false;
        return true;
    }


	public SuperBlock() {
		bitmap = new boolean[Config.MEMORY_SIZE];
		for(int i=0; i< Config.MEMORY_SIZE ; i++) {
			bitmap[i]=false; //aucun block n'est utilise
		}
	}

}