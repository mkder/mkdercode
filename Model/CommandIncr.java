package mkdercode.Model;

import mkdercode.Exception.BnfkException;

/**
 * CLASSE :
 *
 * @author mxm
 *         27/10/16.
 */
public class CommandIncr extends Command{

    public CommandIncr(MemoryInterface m){
        super(m);
    }

    @Override
    public void exec() throws BnfkException {
        incrementCurrentValue();
    }

    private void incrementCurrentValue() throws BnfkException {
        getMemory().setCurrentValue((short) (getMemory().getCurrentValue() + 1));
    }
}
