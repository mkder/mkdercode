package mkdercode.Model;

import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkOutOfDiskException;

/**
 * CLASSE :
 *
 * @author mxm
 *         27/10/16.
 */
public class CommandLeft extends Command{

    public CommandLeft(MemoryInterface m){
        super(m);
    }

    @Override
    public void exec() throws BnfkException {
            pointerLeft();
    }

    private void pointerLeft() throws BnfkException {
        getMemory().setPointer(getMemory().getPointerVal() - 1);
    }
}
