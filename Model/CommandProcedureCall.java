package mkdercode.Model;

import mkdercode.Exception.BnfkException;

/**
 * @author : Rachida
 * Commande qui execute l'appel de procedure
 * change le pointeur de la InstructionsStack
 */
public class CommandProcedureCall extends Command {
    public CommandProcedureCall(MemoryInterface m, InstructionsStack stack) {
        super(m, stack);
    }

    public void exec() throws BnfkException{
        int[] parametres = applyProcedureCallOnStack();
        applyProcedureCallOnMemory(parametres);
    }

    private int[] applyProcedureCallOnStack() throws BnfkException {
        return getStack().callProcedure();
    }

    /**
     * On recupere les variables pointées par notre tableau de parametres.
     * Puis on alloue une nouvelle zone memoire pour notre procedure.
     * Enfin on copie les variables récuperées dans la nouvelle zone memoire.
     * @param parametres - pointeurs relatifs
     * @throws BnfkException
     */
    private void applyProcedureCallOnMemory(int[] parametres) throws BnfkException {
        short[] parametresValues = getValWithRelativePointers(parametres);
        getMemory().allocMemory();
        getMemory().setCurrentValues(parametresValues);
    }

    private short[] getValWithRelativePointers(int[] pointers) throws BnfkException {
        short[] values = new short[pointers.length];
        for(int i = 0; i < pointers.length; i++){
            values[i] = getMemory().getVal(getMemory().getAbsolutePointer(pointers[i]));
        }
        return values;
    }
}