package mkdercode.Model;

import com.google.common.io.Files;
import mkdercode.Constante.Directives;
import mkdercode.Entree.CodeReader;
import mkdercode.Entree.ImageManager;
import mkdercode.Entree.Procedure;
import mkdercode.Entree.ReaderInterface;
import mkdercode.Exception.BnfkBackInfinite;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkJumpInfinite;

import java.util.HashMap;
import java.util.Stack;

/**
 * @author Elie
 *
 * La Classe InstructionsStack est la pile d'instructions de notre machine virtuelle.
 * Les instructions sont stockées dans des StackLink en liste chainée.
 * Le pointeur currentDirective est un des maillons de la chaine.
 *
 * La JumpTable est une HashMap contenant en clé la StackLink actuelle et renvoyant la StackLink associée
 * (Une StackLink JUMP pour une StackLink BACK et vice-versa).
 *
 * Les procedures possedent egalement leur propre HashMap qui possede en clé les noms des procedures
 * définies et retourne la StackLink associée (qui correspond à l'emplacement du début de la definition de la procedure).
 *
 * Enfin une Stack qui contient un historique de pointeurs, previousPointer. On ajoute le pointeur actuel en cas
 * d'appel de procedure. Puis on recupere le dernier element ajouté si on a fini l'appel procedure pour revenir a
 * l'emplacement d'avant l'appel.
 *
 * La liste d'instruction, la JumpTable ainsi que la procedureTable sont créés au debut puis ne seront jamais modifiés.
 */
public class InstructionsStack {
    private ReaderInterface reader;
    private StackLink currentDirective;
    private HashMap<StackLink, StackLink> jumpTable;

    private HashMap<String, StackLink> procedureTable; //Fonctionne comme la jumpTable pour les procedures
    private Stack<StackLink> previousPointers;

    public InstructionsStack(String path) throws BnfkException {
        currentDirective = new StackLink(null);
        jumpTable = new HashMap<>();
        procedureTable = new HashMap<>();
        previousPointers = new Stack<>();

        if (Files.getFileExtension(path).equals("bmp")) {
            this.reader = new ImageManager(path);
        } else this.reader = new CodeReader(path);
        build();
    }

    /**
     * Lit le reader et ajoute une directive dans la liste chainée.
     * @throws BnfkException
     */
    private void updateStack() throws BnfkException {
        Directives newDirectives = reader.getNextDirective();
        StackLink nextDirective = new StackLink(newDirectives);
        currentDirective.setNext(nextDirective);
    }

    /**
     * On passe à l'element suivant de la liste chainée.
     * Puis on retourne sont contenu.
     * @return
     */
    public Directives getNext() {
        currentDirective = currentDirective.getNext();
        return currentDirective.getData();
    }

    /**
     * Methode de construction de l'InstructionStack.
     * Suivant les Directives obtenu, il completera ou non les differents Tables (jumpTable et procedureTable)
     * et ajoutera un parametre au StackLink.
     * Le pointeur sera alors le premier element de la liste sauf si une Directives MAIN est rencontrée.
     * @throws BnfkException
     */
    private void build() throws BnfkException {
        StackLink first = currentDirective;
        Stack<StackLink> jumpStack = new Stack<>();
        do {
            updateStack();
            currentDirective = currentDirective.getNext();

            if (currentDirective.getData() == Directives.JUMP) {
                jumpStack = addJump(jumpStack);
            }
            if (currentDirective.getData() == Directives.BACK) {
                jumpStack = addBack(jumpStack);
            }
            if (currentDirective.getData() == Directives.PROCEDURE_DEFINE) {
                Procedure procedure = (Procedure) Directives.PROCEDURE_DEFINE.getParametres();
                procedureTable.put(procedure.getName(), currentDirective);
            }
            if (currentDirective.getData() == Directives.PROCEDURE_CALL) {
                currentDirective.setParametre(Directives.PROCEDURE_CALL.getParametres());
            }
            if (currentDirective.getData() == Directives.MAIN) {
                first = currentDirective;
            }
            if (currentDirective.getData() == Directives.RETURN) {
                currentDirective.setParametre(Directives.RETURN.getParametres());
            }
        } while (currentDirective.getData() != null);
        currentDirective = first;
    }

    private Stack<StackLink> addJump(Stack<StackLink> jumpStack){
        jumpStack.push(currentDirective);
        return jumpStack;
    }

    private Stack<StackLink> addBack(Stack<StackLink> jumpStack) throws BnfkBackInfinite{
        if(jumpStack.isEmpty())
            throw new BnfkBackInfinite();
        StackLink jump = jumpStack.pop();
        jumpTable.put(jump, currentDirective);
        jumpTable.put(currentDirective, jump);
        return jumpStack;
    }

    public void jumpTo() throws BnfkException {
        if (!jumpTable.containsKey(currentDirective)) {
            if (currentDirective.getData() == Directives.JUMP) throw new BnfkJumpInfinite();
            if (currentDirective.getData() == Directives.BACK) throw new BnfkBackInfinite();
        }
        currentDirective = jumpTable.get(currentDirective);
    }

    /**
     * Prends la procedure en parametre de la StackLink et cherche dans procedureTable
     * son emplacement dans la liste des instructions.
     * Garde l'ancien pointeur dans le pointerStack et retourne la valeur des parametres
     * de l'appel de procedure.
     * @return
     */
    public int[] callProcedure() {
        Procedure procedure = (Procedure)currentDirective.getParametre();
        previousPointers.push(currentDirective);
        currentDirective = procedureTable.get(procedure.getName());
        return procedure.getValParametre();
    }

    /**
     * Prends le parametre de la StackLink qui correspond cette fois-ci au parametre de l'instruction
     * '@return'.
     * Recupere l'ancien pointeur sauvegardé dans le pointerStack.
     * Retourne le paramtre de la StackLink.
     * @return
     * @throws BnfkException - en cas d'absence de Procedure appellée.
     */
    public int returnProcedure() throws BnfkException {
        int res = (int)currentDirective.getParametre();
        if (previousPointers.isEmpty())
            throw new BnfkException("La commande @return est appelée mais pas de procedure appelée.");
        currentDirective = previousPointers.pop();
        return res;
    }
}