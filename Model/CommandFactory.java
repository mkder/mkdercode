package mkdercode.Model;

import mkdercode.Constante.Directives;
import mkdercode.Exception.BnfkException;

/**
 * CLASSE :
 *
 * @author mxm
 *         27/10/16.
 */
public class CommandFactory {
    private MemoryInterface memory; // to give the command access to memory
    private InstructionsStack stack;

    public CommandFactory(MemoryInterface memory, InstructionsStack stack) {
        this.memory = memory;
        this.stack = stack;
    }

    public Command create(Directives d) throws BnfkException{

        switch (d) {
            case INCR:
                return new CommandIncr(memory);
            case DECR:
                return new CommandDecr(memory);
            case RIGHT:
                return new CommandRight(memory);
            case LEFT:
                return new CommandLeft(memory);
            case IN:
                return new CommandIn(memory);
            case OUT:
                return new CommandOut(memory);
            case JUMP:
                return new CommandJump(memory, stack);
            case BACK:
                return new CommandBack(memory, stack);
            case PROCEDURE_CALL:
                return new CommandProcedureCall(memory, stack);
            case RETURN:
                return new CommandReturn(memory, stack);
            default:
                throw new BnfkException("Il est interdit de définir une Procedure " +
                        "après @main.");
        }

    }

}
