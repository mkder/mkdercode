package mkdercode.Model;

import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkOutOfDiskException;
import mkdercode.View.Console;
import mkdercode.Trace.Metrics;

/**
 * Classe mère des Classes Command.
 * Les Classes Command execute les actions sur la Memory et InstructionsStack
 * suivant l'instruction appelée.
 * @author mxm
 */
public class Command {
    private MemoryInterface memory;
    private InstructionsStack stack;
    //Constructeur des commandes
    public Command(MemoryInterface m){
        memory = m;
        Metrics.incr_EXEC_MOVE();//S12 Déplacement du pointeur d'execution = execution d'une nouvelle commande ?
    }

    public Command(MemoryInterface m, InstructionsStack stack){
        memory = m;
        this.stack = stack;
    }
    //getter sur le mémoire pour les classes filles
    protected MemoryInterface getMemory(){
        return memory;
    }

    protected InstructionsStack getStack(){
        return stack;
    }
    //Methode de la commande
    public void exec() throws BnfkException {
        //doit etre surchargée
        Console.printError("exec() doit etre surchargée", 10);
    }

    public String displayMemoryState() throws BnfkOutOfDiskException{
        return "Memory state : \tc["+ getMemory().getPointerVal()+"]:" + getMemory().getCurrentValue();
    }


}
