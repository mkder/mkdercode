package mkdercode.Model;

import mkdercode.Constante.Directives;

/**
 * @author : Rachida
 * 
 * Element de la liste chainé de InstructionsStack.
 * Contient une Directives, l'element suivant ainsi qu'un parametre non défini.
 */
public class StackLink {
    private Directives data;
    private StackLink next;

    private Object parametre;

    StackLink(Directives data){
        this.data = data;
    }

    Directives getData() {
        return data;
    }

    StackLink getNext() {
        return next;
    }

    void setNext(StackLink next) {
        this.next = next;
    }

    public void setParametre(Object parametre){
        this.parametre = parametre;
    }
    public Object getParametre(){
        return parametre;
    }
}
