package mkdercode.Model;

import mkdercode.Constante.Config;
import mkdercode.Exception.*;
import mkdercode.Trace.Metrics;

import java.util.Stack;

/**
 * Classe stockant l'ensemble des variables de la machine virtuelle.
 * La variable disk est le tableau qui contient les valeurs de toutes les varaibles.
 * superblock permet pour chaque case du disk de savoir si elle est utilisée ou non.
 * Enfin il y a un integer pointer qui sert de pointeur.
 *
 * Pour l'implementation des procedures, il a été necessaire d'ajouter
 * une limite qui permet de savoir où se situe l'emplacement de variable le plus élevé
 * déjà utilisée afin de démarrer la memoire locale de la procedure juste après.
 * On ajoute une Stack de pointers et une Stack de limits afin de pouvoir revenir aux états
 * anterieur de la memoire, à la fin de l'appel procedure.
 *
 * Enfin, il est important de differencier les pointeurs relatifs qui dependent d'un contexte
 * (la case 0 pour une procedure est sa première variable locale et pas la case 0 absolue de la memoire)
 * des pointeurs absolues qui sont les vrais positions des variables dans la memoire.
 *
 * @author mxm, Elie
 */
public class Memory implements MemoryInterface {
    private int pointer;//le pointeur sur la case du disk
    private short[] disk;//le support de stockage

    //Procedure
    private int limit; //Limite actuelle de notre memoire (case plus haute utilisée)
    private Stack<Integer> pointerStak; //Historique pointer
    private Stack<Integer> limitStack; //Historique des limites atteintes par le prog et les fonctions

    private SuperBlockInterface superblock; //gestion d'erreur + mesure de la taille de la chaine a afficher

    /**
     * Memory constructor : tested
     */
    public Memory() {
        pointer = 0;
        limit = 0;
        pointerStak = new Stack<>();
        limitStack = new Stack<>();
        limitStack.push(-1);
        pointerStak.push(-1);
        initializeMemory();
    }

    /**
     * Initialise le tableau de case de la memoire ainsi que son superblock associé.
     */
    private void initializeMemory(){
        //on initialise la bitmap
        superblock = new SuperBlock();
        //initialisation de la memoire
        disk = new short[Config.MEMORY_SIZE];
        for (int i = 0; i < Config.MEMORY_SIZE; i++) {
            disk[i] = (short) 0; //aucun block n'est utilise
        }
    }

    /**
     * Retourne sous forme de String l'ensemble des variables utilisées.
     */
    @Override
    public String toString() {
        String line = "";
        for (int i = 0; i < Config.MEMORY_SIZE; i++) {
            if (!superblock.isFreeBlock(i)) {
                line += "[c" + String.valueOf(i) + "]: " + disk[i] + "\n";
            }
        }
        return line;
    }

    @Override
    public int getPointerVal() {
        return pointer;
    }

    @Override
    public void setPointer(int pointer) throws BnfkOutOfDiskException {
        checkIsCorrectPointer(pointer);
        Metrics.incr_DATA_MOVE();
        this.pointer = pointer;
    }

    /**
     * Donne la position absolue d'un pointeur suivant le contexte actuel.
     */
    @Override
    public int getAbsolutePointer(int relativePointer){
        return relativePointer + limitStack.peek() + 1;
    }

    private void checkIsCorrectPointer(int pointer) throws BnfkOutOfDiskException{
        if(pointer >= Config.MEMORY_SIZE)
            throw new BnfkOutOfDiskException("Overflow");
        if(pointer < 0)
            throw new BnfkOutOfDiskException("Underflow");
    }

    @Override
    public short getVal(int pointer) throws BnfkOutOfDiskException{
        checkIsCorrectPointer(pointer);
        Metrics.incr_DATA_READ();//S12 access in memory
        return disk[pointer];
    }
    @Override
    public short getCurrentValue() throws BnfkOutOfDiskException {
        return getVal(pointer);
    }
    private void setValue(int pointer, short value) throws BnfkException{
        checkIsCorrectPointer(pointer);
        if(value > Config.MAX_NUMBER || value < 0)
            throw new BnfkOverFlowException();
        superblock.updateBlock(pointer);
        disk[pointer] = value;
        updateLimit(pointer);
        Metrics.incr_DATA_WRITE();
    }
    @Override
    public void setCurrentValue(short value) throws BnfkException {
        setValue(pointer, value);
    }

    /**
     * Modifie la valeur de la case actuelle et celle des suivantes si il y a
     * plusieurs valeurs données.
     * @param values Nouvelles valeurs pour les cases.
     */
    @Override
    public void setCurrentValues(short[] values) throws BnfkException {
        for(int i = 0; i < values.length; i++){
            setValue(pointer + i, values[i]);
        }
    }

    public int getCopyValueOf(int i) {
        if (i < 0 || i > Config.MEMORY_SIZE) return -1;
        else {
            int res = disk[i];
            return res;
        }
    }

    //Procedure
    private void updateLimit(int pointer){
        if(pointer > limit) limit = pointer;
    }

    /**
     * On sauvegarde l'etat de la memoire dans les Stacks.
     * On met le pointeur actuel une case au-delà de la limite actuelle atteinte.
     * Puis on met à jour la limite.
     */
    @Override
    public void allocMemory() throws BnfkException {
        pointerStak.push(pointer);
        limitStack.push(limit);
        setPointer(limit + 1);
        limit++;
    }

    /**
     * On libere la memoire jusqu'à l'ancienne limite (limite avant appel de procedure).
     * On remet la memoire à son etat precedent.
     */
    @Override
    public void deAllocMemory() throws BnfkException {
        clearMemory(limitStack.peek() + 1, limit + 1);
        limit = limitStack.pop();
        setPointer(pointerStak.pop());
    }

    /**
     * Libere la memoire depuis une position incluse juqu'à une position exclue.
     * @param from Position incluse
     * @param to Position exclue
     */
    private void clearMemory(int from, int to){
        for(int i = from; i < to; i++){
            disk[i] = 0;
            superblock.freeBlock(i);
        }
    }
}