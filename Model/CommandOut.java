package mkdercode.Model;

import mkdercode.Constante.Config;
import mkdercode.Exception.BnfkException;

import java.io.IOException;

/**
 * @author Rachida
 */
public class CommandOut extends Command {

    public CommandOut(MemoryInterface memoryInterface){
        super(memoryInterface);
    }

    @Override
    public void exec() throws BnfkException{
        try{
            if(Config.PATH_OUT == System.out){
                //System.out.println("c[" + action().getPointerVal() + "]: " + (char)action().getCurrentValue());
                System.out.print((char)getMemory().getCurrentValue());
                //System.out.print(getMemory().getCurrentValue());
            }
            else {
                Config.PATH_OUT.write((char)getMemory().getCurrentValue());
            }
        }catch (IOException e){
            throw new BnfkException(e.getMessage());
        }

    }
}
