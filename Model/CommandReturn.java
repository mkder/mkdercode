package mkdercode.Model;

import mkdercode.Constante.Config;
import mkdercode.Exception.BnfkException;

/**
 * @author : Rachida
 * Commande pour l'instruction @return 
 * qui revient au pointer gardé en memoire par la InstructionsStack
 */
public class CommandReturn extends Command {

    public CommandReturn(MemoryInterface m, InstructionsStack stack) {
        super(m, stack);
    }

    public void exec() throws BnfkException{
        int parametre = applyReturnOnStack();
        applyReturnOnMemory(parametre);
    }

    private int applyReturnOnStack() throws BnfkException{
        return getStack().returnProcedure();
    }

    /**
     * On recuper la variable du parametre (pointeur relatif).
     * On libere la memoire locale.
     * On change la variable actuelle par la variable recuperée au début.
     * @param parametre - pointeur relatif
     * @throws BnfkException
     */
    private void applyReturnOnMemory(int parametre) throws BnfkException{
        short parametreValue = -1;
        if(checkThereIsParameter(parametre)){
            int pos = getMemory().getAbsolutePointer(parametre);
            parametreValue = getMemory().getVal(pos);
        }
        getMemory().deAllocMemory();
        if(checkThereIsParameter(parametre))
            getMemory().setCurrentValue(parametreValue);
    }

    private boolean checkThereIsParameter(int parameter){
        return parameter != -Config.MEMORY_SIZE - 1;
    }
}