package mkdercode.Model;

/**
 * Created by mxm on 28/09/16.
 */
public interface SuperBlockInterface {
    public boolean updateBlock(int blockNumber) ;
    public boolean freeBlock(int blockNumber);

    /**
     * isFreeBlock dit si un bloc est libre
     * @return true si le bloc est libre, false s'il a deja ete alloue dans le programme
     * @param blockNumber : int
     */
    public boolean isFreeBlock(int blockNumber) ;

}
