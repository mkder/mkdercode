package mkdercode.Model;

import mkdercode.Exception.BnfkException;

/**
 * CLASSE :
 *
 * @author mxm
 *         27/10/16.
 */
public class CommandRight extends Command{
    public CommandRight(MemoryInterface m){
        super(m);
    }

    @Override
    public void exec() throws BnfkException {
        pointerRight();
    }

    private void pointerRight() throws BnfkException {
        getMemory().setPointer(getMemory().getPointerVal() + 1);
    }
}
