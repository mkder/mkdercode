package mkdercode.Model;

import mkdercode.Exception.BnfkException;

/**
 * Created by Elie on 18/11/2016.
 */
public class CommandJump extends Command {
    public CommandJump(MemoryInterface m, InstructionsStack stack) {
        super(m, stack);
    }

    public void exec() throws BnfkException{
        if(getMemory().getCurrentValue() != 0){
            return;
        }
        getStack().jumpTo();
    }
}
