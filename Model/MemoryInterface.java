package mkdercode.Model;

import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkOutOfDiskException;

/**
 * Created by mxm on 28/09/16.
 */
public interface MemoryInterface {
    int getPointerVal();
    void setPointer(int pointer) throws BnfkOutOfDiskException;
    int getAbsolutePointer(int relativePointer);
    short getVal(int pointer) throws BnfkOutOfDiskException;
    short getCurrentValue() throws BnfkOutOfDiskException;
    void setCurrentValue(short value) throws BnfkException;
    void setCurrentValues(short[] values) throws BnfkException;

    //Procedures
    void allocMemory() throws BnfkException;
    void deAllocMemory() throws BnfkException;

    String toString();
}