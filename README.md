#Actuellement
##Demarrage
La Classe VM dans le package Controler reconnait les arguments donnés par
l'utilisateur puis demarrer les bons ExecutableInterface.
##Lecture du fichier
###ReaderInterface
Les ReaderInterface dans le package Entree lit le fichier donné en parametre
ligne par ligne. Il utilise la Classe Enum Directives pour reconnaitre les
instructions lut et la Classe MacroReader pour les macros (la definition
puis reconnaissance des macros déjà sauvegardées).
MacroReader n'est utilisé que par la Classe CodeReader et non par ImageManager.

##Execution
###ExecutableInterface
Suivant les arguments donnés au demarrage la VM va lancer les bon ExecutableInterface.
Checker pour l'argument --check.
Rewriter pour --rewrite.
Translator pour --translate.
Et enfin Interpretor si aucun arguments n'est donné. C'est aussi la seul
Classe qui va utilisé la memoire et la pile d'execution.
###Memoire
La memoire est contenu dans la Classe Memory. Elle est demarrée au debut 
de Interpretor. Elle contient un tableau de 30 000 short qui representeront
les variables de notre machine virtuelle, un Integer qui sera le pointer de la memoire
,ainsi que les methode utiles pour la modification des variables et du pointer.
###Pile d'execution
Juste apres la lecture les instructions reconnues sont envoyées sont 
forme de Directives à une Classe Stack. Celle-ci enregistre les Directives
dans une liste chaînée ainsi que la jump table dans une HashMap.
Elle met a jour, apres chaque execution, la Directives actuelle puis la renvoie
à l'Interpretor.
###Execution des instructions
Une fois que l'Interpretor reçoit une Directives, il appelle une Classe 
CommandFactory qui lui renvoi la Classe Command associé à l'instruction
en cours. Puis on execute cette Classe.
###Command
Les Classes qui herite de Command possedent toute une methode exec
pour executer sa fonction. Elle va ensuite appeller les methodes dans Memory
ou Stack suivant les besoins.
##Trace
###Metrics
Les metrics sont contenue dans la Classe Metrics en static. On appelle
ensuite les methodes de Metrics quand on doit mettre a jour une metrics.
###Logger
La Classe Logger permet de garder une trace de l'execution du programme.



#Historique des modification
#Level 1


#Level 2
##Supporting shortened syntax
###Classe
Une Classe Enum Directives a été ajoutée.
Celle-ci contient la version longue et la version courte de chaque
instruction supportée. Les methodes de cette classe permettent de 
reconnaitre une instruction à partir d'une String ou de renvoyer la version
desirée.
Une Classe Arguments dans le package Constante qui garde en memoire les 
String des arguments.
 
##Rewriting instructions
###Classe
Pas de nouvelle classe
###Modification
La Classe Interpretor doit reconnaitre les arguments donnés et si il detecte
"--rewrite" il execute une methode argRewrite qui renvoi à la Console
 les Strings de la version courte des instructions reconnu grâce aux Directives.
 
##Using images to represent programs
 

#Level 3
##Introducing metrics
###Classe
Un package Trace a été créé pour l'occasion.
La Classe Metrics s'occupe de la modification des metrics qui seront
rendues en fin d'execution. Chaque metrics correspond à une variable 
static contenue dans cette classe. Pour les modifier, on fait appelle
aux methodes "incr_" et "start_" de cette classe.
A la fin, de l'execution on appelle la methodes display() qui retourne
une String contenant les resultats.

###Modifications
Les appelles de methodes ont été ajoutés dans les Classes correspondant;
Translator, CodeReader, ImageManager, VM, Checker, Command, Memory et 
Console.

##Logging steps to trace execution
###Classe
Une nouvelle Classe Logger a été créée.
C'est Interpretor qui va appeller la methode log pour sauvegarder la 
ligne en cours de lecture si necessaire

##Code macros
###Classe
Deux classes créées; Macro et MacroReader dans le package Entree.
La classe Macro garde juste le nom et la liste des Directives de la Macro.
MacroReader recuper la ligne de la definition de la Macro puis la
parse afin de creer un nouvel objet Macro.

###Modification
La Classe CodeReader doit maintenant reconnaitre les debuts et fin de
definition de Macro ("@define" et "end").
De plus, il doit aussi detecter les Macro à l'interieur du code grâce
à la liste de Macro créée par MacroReader et la remplacer par sa liste 
de Directives.

##Parameterized macro and recursive expansion
###Classe
Pas de nouvelle classe
###Modification
MacroReader doit maintenant detecter la presence ou non de parametre ainsi
que la presence de Macro precedemment defini dans la definition de Macro
qu'elle parse actuelement.

##Interpretation optimization
###Classe
Pas de nouvelle classe
###Modification
La Classe Stack utilise à present une liste chaînée pour sauvegarder
les Directives données par le Reader.
La jump table est une HashMap ajouté qui garde les Chainons et associe
les JUMP et BACK rencontrés durant la construction de la Stack.