package mkdercode.CodeGenerator;

import mkdercode.Controler.ExecutableInterface;
import mkdercode.Entree.Macro;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkSyntaxException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Cette classe hérite du CodeGenerator.Elle utilise le constructeur de sa classe mère pour spécifier la syntaxe du langage Java.
 * Elle implémente ExecutableInterface afin de pouvoir être appellé par la vm.
 */

public class CodeGeneratorJ extends CodeGenerator implements ExecutableInterface {
    private Map<String,ArrayList<String>> macros;
    public CodeGeneratorJ(String path) throws BnfkException {
        super(path,".java",new String[]{"int t[] = new int[30000];","int ptr = 0;" ,"for(int i = 0; i < 30000; i++){t[i] = 0;}"},
                new String[]{"ptr++;","ptr--;","t[ptr]++;","t[ptr]--;","while(t[ptr]>0){","}","t[ptr] = sc.nextInt();","System.out.print((char)t[ptr]);"},
                "public static void main(String argv[]){\nScanner sc = new Scanner(System.in);"
                ,new String[]{"{","}","(",")","t[ptr]","=","return",";","public static int ","public static void "});
        macros = new HashMap<>();
    }

    @Override
    public void exec() throws BnfkException {
        writer.println("import java.util.*;");
        writer.println("public class Main {");
        super.init();
        writer.println("}");
        writer.println("}");
        writer.close();
    }

    @Override
    protected void writeMacro(Macro macro) throws BnfkSyntaxException {
        macros.put(macro.getName(), (ArrayList<String>) macro.getBody().clone());
        writer.println();
    }

    @Override
    protected void setReadMacro(List<String> lineParsed) throws BnfkSyntaxException {
        if(!macroReader.checkIsValidMacroLine(lineParsed))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + lineParsed.get(0));
        int parametre = 1;
        if(lineParsed.size() == 2)
            parametre = Integer.parseInt(lineParsed.get(1));
        println("for(int i = 0; i < " + parametre + "; i++) {");
        System.out.println(lineParsed);
        for(String line : macros.get(lineParsed.get(0))){
            parse(line);
        }
        println("}");
    }
}
