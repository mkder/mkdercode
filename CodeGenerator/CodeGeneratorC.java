package mkdercode.CodeGenerator;

import mkdercode.Controler.ExecutableInterface;
import mkdercode.Exception.BnfkException;

/**
 * Cette classe hérite du CodeGenerator.Elle utilise le constructeur de sa classe mère pour spécifier la syntaxe du langage C.
 * Elle implémente ExecutableInterface afin de pouvoir être appellé par la vm.
 */
public class CodeGeneratorC extends CodeGenerator implements ExecutableInterface{

    public CodeGeneratorC(String path) throws BnfkException {
        super(path,".c",new String[]{"int *t = malloc(sizeof(int) * 30000);", "for(int i = 0; i < 30000; i++)", "t[i] = 0;", "int *ptr = t;"},
                new String[]{"ptr++;","ptr--;","(*ptr)++;","(*ptr)--;","while(*ptr){","}","(*ptr) = getchar();","putchar(*ptr);"},
                "int main() {"
                ,new String[]{"{","}","(",")","*ptr","=","return",";","int ","void "});
    }

    @Override
    public void exec() throws BnfkException {
        writer.println("#include <stdlib.h>");
        writer.println("#include <stdio.h>");
        super.init();
        writer.println("return 0;");
        writer.println("}");
        writer.close();
    }



}
