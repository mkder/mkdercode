package mkdercode.CodeGenerator;

import com.google.common.io.Files;
import mkdercode.Constante.Config;
import mkdercode.Constante.Directives;
import mkdercode.Entree.*;
import mkdercode.Exception.BnfkException;
import mkdercode.Exception.BnfkFileNotFoundException;
import mkdercode.Exception.BnfkSyntaxException;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Pour le parsing elle herite de LineParser.
 * La detection des elements se fait donc de la meme maniere en revanche
 * au lieu de stocker dans une queue ou de renvoyer des Directives, CodeGenerator
 * va ecrire le code correspondant dans le fichier destination.
 */
public class CodeGenerator extends LineParser{
    BufferedReader reader;
    PrintWriter writer;
    int lineNb;
    String path;
    boolean endOfFile;
    boolean mainNecessary;
    HashMap<String, Boolean> isProcedure;
    boolean writtingMacro;
    boolean writtingProcedure;
    String[] init;
    private String[] instructions;
    String main;
    String[] syntaxe;
    String delimiteurFoncOpen;
    String delimiteurFoncClose;
    String delimiteurVarOpen;
    String delimiteurVarClose;
    String currentCell;
    String egal;
    String returnS;
    String eof;
    String functionDef;
    String procDef;

    public CodeGenerator(String path,String extension, String[] init, String[] instructions,String main,String[] syntaxe) throws BnfkException{
        lineNb = 0;
        endOfFile = false;
        mainNecessary = true;
        writtingMacro = false;
        writtingProcedure = false;
        this.path = path;
        String destination = path.split("\\.", 2)[0] + extension;
        macroReader = new MacroReader();
        procedureReader = new ProcedureReader();
        isProcedure = new HashMap<>();
        this.init = init;
        this.instructions = instructions;
        this.main = main;
        this.syntaxe = syntaxe;
        delimiteurFoncOpen = syntaxe[0];
        delimiteurFoncClose = syntaxe[1];
        delimiteurVarOpen = syntaxe[2];
        delimiteurVarClose = syntaxe[3];
        currentCell = syntaxe[4];
        egal = syntaxe[5];
        returnS = syntaxe[6];
        eof = syntaxe[7];
        functionDef = syntaxe[8];
        procDef = syntaxe[9];
        try{
            reader = new BufferedReader(new FileReader(path));
            writer = new PrintWriter(new BufferedWriter(new FileWriter( new File(destination))));
        }catch (IOException e){
            throw new BnfkException();
        }
    }

    private String readFile() throws BnfkFileNotFoundException {
        String line;
        try {
            line = reader.readLine();
            if (line == null) {
                endOfFile = true;
                reader.close();
                return null;
            }
            lineNb++;
            line = removeComment(line);
            return line;
        } catch (IOException e) {
            throw new BnfkFileNotFoundException(path);
        }
    }

    private void readeImage() throws BnfkException{
        ReaderInterface readerImage;
        Directives directives;
        readerImage = new ImageManager(path);
        writer.println(main);
        for(String i : init){
            writer.println(i);
        }
        while ((directives = readerImage.getNextDirective()) != null) {
            writer.println(instructions[Integer.parseInt(directives.getGeneratingCode())]);
        }
    }

    public void init() throws BnfkException {
        try {
            if (Files.getFileExtension(path).equals("bmp")) {
                readeImage();
            } else {
                while (!endOfFile) {
                    parse(readFile());
                }
            }
        } catch (BnfkSyntaxException e) {
            throw new BnfkSyntaxException(lineNb, e.getMessage());
        }
    }

    private String removeComment(String line) {
        if (line.contains("#")) return line.substring(0, line.indexOf("#"));
        return line;
    }

    protected void println(String line){
        if(writtingMacro){
            writer.println(line + "\\");
        }else {
            writer.println(line);
        }
    }

    @Override
    protected void setReturn(List<String> lineParsed) throws BnfkSyntaxException {
        int parametre = procedureReader.getReturn(lineParsed);
        writtingProcedure = false;
        if(parametre != -Config.MEMORY_SIZE - 1)
            println("return t[" + parametre + "];");
        else
            println(returnS+eof);
        println(delimiteurFoncClose);
    }

    @Override
    protected void setNameMacro(List<String> lineParsed) throws BnfkSyntaxException {
        mainNecessary = false;
        Macro macro = macroReader.nameMacro(lineParsed);
        if(macro != null)
            writeMacro(macro);
        else
            inMacro = true;
    }

    @Override
    protected void setDefineMacro(List<String> lineParsed) throws BnfkSyntaxException {
        Macro macro = macroReader.defineMacro(lineParsed);
        if(macro != null){
            inMacro = false;
            writeMacro(macro);
        }
    }

    protected void writeMacro(Macro macro) throws BnfkSyntaxException{
        if(writtingProcedure){
            writer.println("}");
            writtingProcedure = false;
        }
        writer.print("#define " + macro.getName() + "() ");
        ArrayList<String> macroBody = macro.getBody();
        writtingMacro = true;
        for(String line : macroBody){
            parse(line);
        }
        writtingMacro = false;
        writer.println();
    }

    @Override
    protected void setReadMacro(List<String> lineParsed) throws BnfkSyntaxException {
        if(!macroReader.checkIsValidMacroLine(lineParsed))
            throw new BnfkSyntaxException(BnfkSyntaxException.CALL_INSTRUCTION + lineParsed.get(0));
        int parametre = 1;
        if(lineParsed.size() == 2)
            parametre = Integer.parseInt(lineParsed.get(1));
        println("for(int i = 0; i < " + parametre + "; i++) {" + lineParsed.get(0) + "();}");
    }

    @Override
    protected void setDefineFunction(List<String> lineParsed) throws BnfkSyntaxException {
        mainNecessary = false;
        Procedure procedure = procedureReader.nameProcedure(lineParsed);
        isProcedure.put(procedure.getName(), false);
        String buffer = functionDef + procedure.getName() + delimiteurVarOpen;
        writeProcedure(procedure, buffer);
    }

    @Override
    protected void setDefineProcedure(List<String> lineParsed) throws BnfkSyntaxException {
        mainNecessary = false;
        Procedure procedure = procedureReader.nameProcedure(lineParsed);
        isProcedure.put(procedure.getName(), true);
        String buffer = procDef+ procedure.getName() + delimiteurVarOpen;
        writeProcedure(procedure, buffer);
    }

    protected void writeProcedure(Procedure procedure, String buffer){
        if(writtingProcedure){
            writer.println(delimiteurFoncClose);
        }
        writtingProcedure = true;
        int nbVar = procedure.getNbParametre();
        if(nbVar != 0){
            buffer += "int var0";
            for(int i = 1; i < nbVar; i++){
                buffer += ", int var" + String.valueOf(i);
            }
        }
        buffer += delimiteurVarClose+delimiteurFoncOpen;
        writer.println(buffer);
        Arrays.asList(init).forEach(s-> writer.println(s));
        for(int i = 0; i < nbVar; i++){
            writer.println("t[" + i + "] = var" + i + eof);
        }
    }

    @Override
    protected void setReadProcedure(List<String> lineParsed) throws BnfkSyntaxException {
        if(writtingProcedure){
            //writer.println(delimiteurFoncClose);
        }
        Procedure procedure = procedureReader.readProcedure(lineParsed);
        String buffer = procedure.getName() + delimiteurVarOpen;
        int[] valVar = procedure.getValParametre();
        if(valVar.length != 0){
            buffer += "t[" + valVar[0]+ "]";
            for(int i = 1; i < valVar.length; i++){
                buffer += ", t[" + valVar[i] + "]";
            }
        }
        buffer += delimiteurVarClose + eof;
        if(!isProcedure.get(lineParsed.get(0)))
            buffer = currentCell + egal + buffer;
        println(buffer);
    }

    @Override
    protected void setLongInstruction(List<String> line) throws BnfkSyntaxException {
        if(mainNecessary || Directives.MAIN.isTheDirective(line.get(0))){
            if(writtingProcedure){
                writer.println(delimiteurFoncClose);
                writtingProcedure = false;
            }
            writer.println(main);
            for(String i : init)
                writer.println(i);
            mainNecessary = false;
            if(Directives.MAIN.isTheDirective(line.get(0)))
                return;
        }
        println(instructions[Integer.parseInt(Directives.toDirective(line.get(0)).getGeneratingCode())]);
    }

    @Override
    protected void setShortInstruction(List<String> line) throws BnfkSyntaxException {
        if(mainNecessary){
            writer.println(main);
            for(String i : init)
                writer.println(i);
            mainNecessary = false;
        }
        for (String word : line) {
            for (int i = 0; i < word.length(); i++) {
                if (!Directives.isShort((String.valueOf(word.charAt(i)))))
                    throw new BnfkSyntaxException();//TODO
                println(instructions[Integer.parseInt(Directives.toDirective(String.valueOf(word.charAt(i))).getGeneratingCode())]);
            }
        }
    }
}
