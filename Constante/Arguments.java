package mkdercode.Constante;

/**
 * Classe stockant les Strings des arguments disponibles.
 * @author Elie
 */
public final class Arguments {
    final public static String READFILE = "-p";
    final public static String REWRITE = "--rewrite";
    final public static String TRANSLATE = "--translate";
    final public static String OUT = "-o";
    final public static String IN = "-i";
    final public static String CHECK = "--check";
    final public static String TRACE = "--trace";
    final public static String GENERATE_C = "--generateC";
    final public static String GENERATE_J = "--generateJ";
}
