package mkdercode.Constante;

/**
 * Cette enum contient les differentes versions des instructions si elle existe.
 * @author mxm, Elie
 */
public enum Directives {
    RIGHT("RIGHT", ">", "#0000FF", "0"),//on ajoute les codes RGB en position 2
    LEFT("LEFT", "<", "#9400D3", "1"),
    INCR("INCR", "+", "#FFFFFF", "2"),
    DECR("DECR", "-", "#4B0082", "3"),
    JUMP("JUMP", "[", "#FF7F00", "4"),
    BACK("BACK", "]", "#FF0000", "5"),
    IN("IN", ",", "#FFFF00", "6"),
    OUT("OUT", ".", "#00FF00", "7"),
    //On ajoute l'ensemble des mots system
    VOID(),
    PROCEDURE_DEFINE(),
    PROCEDURE_CALL(),
    RETURN("@return"),
    MAIN("@main"),
    DEFINE("@define"),
    FUNCTION("@function"),
    PROCEDURE("@procedure"),
    END("end");

    private String[] values;

    Directives(String... vals) {
        this.values = vals;
    }

    /**
     * Retourne la Directives correspondante.
     * @param str Directives.
     */
    public static Directives toDirective(String str) {
        for (Directives a : Directives.values()) {
            if (a.isTheDirective(str)) {
                return a;
            }
        }
        return null;
    }

    /**
     * Verifie l'existance de cette instruction.
     */
    public static boolean exists(String target) {
        for (Directives a : Directives.values()) {
            if (a.isTheDirective(target)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Confirme que cette String est bien la Directive donnée.
     */
    public boolean isTheDirective(String directive) {
        for (String word : values) {
            if (directive.equals(word))
                return true;
        }
        return false;
    }

    public String getLong() {
        if (values.length > 0)
            return values[0];
        return null;
    }

    public String getShort() {
        if (values.length > 1)
            return values[1];
        return null;
    }

    public String getRGB() {
        if (values.length > 2)
            return values[2];
        return null;
    }

    public String getGeneratingCode() {
        if (values.length > 3)
            return values[3];
        return null;
    }

    public static boolean isLong(String target) {
        for (Directives a : Directives.values()) {
            if (a.values.length > 0 && a.values[0].equals(target)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isShort(String target) {
        for (Directives a : Directives.values()) {
            if (a.values.length > 1 && a.values[1].equals(target)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isRGB(String target) {
        for (Directives a : Directives.values()) {
            if (a.values.length > 2 && a.values[2].equals(target)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Parametre non defini. Utile pour transmettre des informations non static.
     */
    Object parametres;

    public void setParametres(Object parametres) {
        this.parametres = parametres;
    }

    public Object getParametres() {
        return parametres;
    }
}
