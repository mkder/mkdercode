package mkdercode.Constante;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Contient un ensemble d'elements static utilisables dans l'ensemble de la
 * machine virtuelle.
 */
public final class Config {
    public static String LOG_PATH = "bnfk_log.txt";
    public static int MEMORY_SIZE = 30000;
	public static Short MAX_NUMBER = 255;
    public static InputStream PATH_IN = System.in;
    public static OutputStream PATH_OUT = System.out;
    public static int INSTR_SIZE = 3;//taille d'une instruction dans l'image en pixel
    public static String IMAGE_PATH = "test.bmp";
}
